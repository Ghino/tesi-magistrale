﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClassSymbolRotator : MonoBehaviour
{
    public float yRotationSpeed = 10f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // make the icon spin above the characters/look at the main camera
    void FixedUpdate()
    {
        transform.LookAt(Camera.main.transform.position);
    }
}
