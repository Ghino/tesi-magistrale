﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUILog : MonoBehaviour
{
    public bool showLog = false;
    private ScrollRect scroll;
    private GameObject text;
    private int lineCounter = 0;

    // Start is called before the first frame update
    void Start()
    {
        scroll = GetComponent<ScrollRect>();
        text = transform.GetChild(0).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddLog(string newLog)
    {
        if (!showLog)
        {
            return;
        }
        lineCounter++;
        if (lineCounter >= 7)
        {
            //float width = text.GetComponent<RectTransform>().sizeDelta.x;
            //float height = text.GetComponent<RectTransform>().sizeDelta.y + text.GetComponent<Text>().fontSize;
            //text.GetComponent<RectTransform>().sizeDelta = new Vector2(width, height);
            ResetLog();
            lineCounter = 0;
        }
        text.GetComponent<Text>().text = text.GetComponent<Text>().text + "\n" + newLog;
    }

    public void ResetLog()
    {
        text.GetComponent<Text>().text = "";
    }
}
