﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

//scriptable object to store all the elements and values regarding a skill
[CreateAssetMenu(fileName = "Move", menuName = "ScriptableObjects/Moves", order = 1)]
public class Move : ScriptableObject
{
    //common elements
    public string moveName;
    public bool hasCastTime;
    public float castTime;
    public GameObject castParticle;
    public float cooldown;
    public MoveCategory category;
    public MoveRangeType rangeType;

    //rangeType ranged
    public float maxRange;

    //attack category
    public float damage;
    public float attackSpeed;

    //ranged attack
    public bool isParabolic;
    public GameObject projectile;
    
    //public Transform originPosition;

    //AOE attack
    public GameObject attackPattern;

    //heal category
    public float healPotency;
    public GameObject healParticle;
    
    //buff_debuff category
    public float BDValue; //percentage value, negative values for debuffs
    public float BDDuration;
    public AffectedStat affectedStat;
    public GameObject effectParticle;

    //movement category
    public float distance;
    public float movPower;
    public MovementDirection direction;

    public string executorName;

    //public float reward;

    //used for copies at runtime
    public void Initialize(Move move)
    {
        this.moveName = move.moveName;
        this.hasCastTime = move.hasCastTime;
        this.castTime = move.castTime;
        this.castParticle = move.castParticle;
        this.cooldown = move.cooldown;
        this.category = move.category;
        this.rangeType = move.rangeType;
        this.damage = move.damage;
        this.attackSpeed = move.attackSpeed;
        this.isParabolic = move.isParabolic;
        this.projectile = move.projectile;
        this.maxRange = move.maxRange;
        this.attackPattern = move.attackPattern;
        this.healPotency = move.healPotency;
        this.healParticle = move.healParticle;
        this.BDValue = move.BDValue;
        this.BDDuration = move.BDDuration;
        this.affectedStat = move.affectedStat;
        this.effectParticle = move.effectParticle;
        this.executorName = move.executorName;
    }
}

//category of the skill
public enum MoveCategory { Attack, Heal, Movement, Buff_Debuff }

//range/area of effect of the skill
public enum MoveRangeType { Melee, Ranged, AOE }

//for buffs/debuffs, indicates which stat is affected
public enum AffectedStat { Attack, Defense }

//for movement skills, indicates the direction from the player
public enum MovementDirection { Forward, Back, Left, Right }

#if UNITY_EDITOR

[CustomEditor(typeof(Move))]
public class MoveEditor : Editor
{
    override public void OnInspectorGUI()
    {
        Move myScript = target as Move;

        myScript.moveName = EditorGUILayout.TextField("Name", myScript.moveName);
        myScript.hasCastTime = GUILayout.Toggle(myScript.hasCastTime, "Has cast time");
        if (myScript.hasCastTime)
        {
            myScript.castTime = EditorGUILayout.FloatField("Cast time", myScript.castTime);
            myScript.castParticle = (GameObject)EditorGUILayout.ObjectField("Cast Particle", myScript.castParticle, typeof(GameObject), true);
        }
        myScript.cooldown = EditorGUILayout.FloatField("Cooldown", myScript.cooldown);
        myScript.category = (MoveCategory)EditorGUILayout.EnumPopup("Move category", myScript.category);
        myScript.rangeType = (MoveRangeType)EditorGUILayout.EnumPopup("Move range type", myScript.rangeType);

        if (myScript.category.Equals(MoveCategory.Attack))
        {
            myScript.damage = EditorGUILayout.FloatField("Damage", myScript.damage);
            myScript.attackSpeed = EditorGUILayout.FloatField("Attack Speed", myScript.attackSpeed);
            
            if (myScript.rangeType.Equals(MoveRangeType.Ranged))
            {
                myScript.isParabolic = GUILayout.Toggle(myScript.isParabolic, "Is parabolic");
                myScript.projectile = (GameObject)EditorGUILayout.ObjectField("Projectile", myScript.projectile, typeof(GameObject), true);
                myScript.maxRange = EditorGUILayout.FloatField("Max range", myScript.maxRange);
                //myScript.originPosition = (Transform)EditorGUILayout.ObjectField("Origin Pos", myScript.originPosition, typeof(Transform), true);
            }

            if (myScript.rangeType.Equals(MoveRangeType.AOE))
            {
                myScript.attackPattern = (GameObject)EditorGUILayout.ObjectField("Attack Pattern", myScript.attackPattern, typeof(GameObject), true);
            }
        }

        if (myScript.category.Equals(MoveCategory.Heal))
        {
            myScript.healPotency = EditorGUILayout.FloatField("Heal potency", myScript.healPotency);
            myScript.healParticle = (GameObject)EditorGUILayout.ObjectField("Heal Particle", myScript.healParticle, typeof(GameObject), true);
            if (myScript.rangeType.Equals(MoveRangeType.Ranged) || myScript.rangeType.Equals(MoveRangeType.AOE))
            {
                myScript.maxRange = EditorGUILayout.FloatField("Max range", myScript.maxRange);
            }
        }
        
        if (myScript.category.Equals(MoveCategory.Buff_Debuff))
        {
            myScript.affectedStat = (AffectedStat)EditorGUILayout.EnumPopup("Affected Stat", myScript.affectedStat);
            myScript.BDValue = EditorGUILayout.FloatField("Value", myScript.BDValue);
            myScript.BDDuration = EditorGUILayout.FloatField("Duration", myScript.BDDuration);
            myScript.effectParticle = (GameObject)EditorGUILayout.ObjectField("Effect Particle", myScript.effectParticle, typeof(GameObject), true);
            if (myScript.rangeType.Equals(MoveRangeType.Ranged))
            {
                myScript.maxRange = EditorGUILayout.FloatField("Max range", myScript.maxRange);
            }
        }

        if (myScript.category.Equals(MoveCategory.Movement))
        {
            myScript.distance = EditorGUILayout.FloatField("Distance", myScript.distance);
            myScript.movPower = EditorGUILayout.FloatField("Movement Power", myScript.movPower);
            myScript.direction = (MovementDirection)EditorGUILayout.EnumPopup("Movement Direction", myScript.direction);
            if (myScript.rangeType.Equals(MoveRangeType.Ranged))
            {
                myScript.maxRange = EditorGUILayout.FloatField("Max range", myScript.maxRange);
            }
        }

        myScript.executorName = EditorGUILayout.TextField("Name of the script", myScript.executorName);

        //myScript.reward = EditorGUILayout.FloatField("Reward", myScript.reward);

        if (GUI.changed)
        {
            EditorUtility.SetDirty(myScript);
        }
    }
}

#endif