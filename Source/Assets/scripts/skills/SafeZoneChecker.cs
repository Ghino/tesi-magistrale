﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//used to check which safe zones are safe from the force aoe, based on its source
public class SafeZoneChecker : MonoBehaviour
{
    public bool isSafe { get; private set; } = false;
    private bool insideForceZone = false;
    private Collider forceCollider;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void FixedUpdate()
    {
        if (insideForceZone && forceCollider == null)
        {
            insideForceZone = false;
            isSafe = false;
        }

        //if the aoe is ready
        if (insideForceZone)
        {
            LayerMask arenaLayer = (1 << 11);
            Vector3 end = new Vector3(forceCollider.transform.position.x, forceCollider.transform.position.y + 2, forceCollider.transform.position.z);
            if (Physics.Linecast(gameObject.transform.position, end, out RaycastHit hit, arenaLayer))
            {
                isSafe = true;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("ForceZone"))
        {
            insideForceZone = true;
            forceCollider = other;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag.Equals("ForceZone"))
        {
            insideForceZone = true;
            forceCollider = other;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag.Equals("ForceZone"))
        {
            insideForceZone = false;
            forceCollider = null;
            isSafe = false;
        }
    }
}
