﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//handles each area of a certain pattern, checking the presence of players inside and activating the navmeshobstacle if none are inside
public class PatternMarker : MonoBehaviour
{
    public GameObject visualEffect;
    private NavMeshObstacle ob;
    private int numOfAiInside = 0;
    private List<GameObject> charactersInside = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        if(gameObject.GetComponent<NavMeshObstacle>() != null)
        {
            ob = gameObject.GetComponent<NavMeshObstacle>();
            ob.enabled = false;
        }
    }

    private void FixedUpdate()
    {
        //if it isn't the force aoe, since that one doesn't have the navmeshobstacle
        if(ob != null)
        {
            if (numOfAiInside <= 0)
            {
                ob.enabled = true;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("AIPlayer"))
        {
            numOfAiInside++;
            charactersInside.Add(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag.Equals("AIPlayer"))
        {
            numOfAiInside--;
            for (int i = charactersInside.Count - 1; i >= 0; i--)
            {
                if (charactersInside[i].name.Equals(other.gameObject.name))
                {
                    charactersInside.RemoveAt(i);
                }
            }
        }

        if (numOfAiInside <= 0)
        {
            if (gameObject.GetComponent<NavMeshObstacle>() != null)
            {
                ob.enabled = true;
            }
        }
    }

    public List<GameObject> GetAffectedCharacters()
    {
        List<GameObject> charactersAffected = new List<GameObject>();
        foreach(GameObject character in charactersInside)
        {
            //there is a possibility where a character inside the marker dies before the activation of the skill, resulting in his gameobject disabled
            if(character.activeSelf)
            {
                charactersAffected.Add(character);
            }
        }
        return charactersAffected;
    }

    public void Trigger(GameObject parent)
    {
        if(parent != null)
        {
            visualEffect = Instantiate(visualEffect, gameObject.transform.position, Quaternion.identity, parent.transform);
        }
        else
        {
            visualEffect = Instantiate(visualEffect, gameObject.transform.position, Quaternion.identity);
        }
    }
}
