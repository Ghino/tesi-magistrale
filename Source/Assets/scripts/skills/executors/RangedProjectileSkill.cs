﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//projectile skill
public class RangedProjectileSkill : SkillExecutor
{
    public override void Initialize(Move m)
    {
        this.m = m;
        cd = 0;
    }

    public override ExecutorErrorCodes CanExecute(List<GameObject> targets)
    {
        if (gameObject.GetComponent<PersonaController>().isCasting)
        {
            return ExecutorErrorCodes.Cast;
        }

        if (Time.time >= cd)
        {
            if (targets == null)
            {
                return ExecutorErrorCodes.OK;
            }
            if (Vector3.Distance(gameObject.transform.position, targets[0].transform.position) < m.maxRange)
            {
                if (CheckLineOfSight(targets[0]))
                {
                    return ExecutorErrorCodes.OK;
                }
                else
                {
                    return ExecutorErrorCodes.Sight;
                }
            }
            else
            {
                return ExecutorErrorCodes.Distance;
            }
        }
        else
        {
            return ExecutorErrorCodes.CD;
        }
    }

    public override bool ExecuteSkill(List<GameObject> targets)
    {
        if (CanExecute(targets).Equals(ExecutorErrorCodes.OK))
        {
            Move modifiedMove = ScriptableObject.CreateInstance<Move>();
            modifiedMove.Initialize(m);
            float modifierValue = gameObject.GetComponent<PersonaController>().statsModifiers[AffectedStat.Attack];
            modifiedMove.damage = m.damage + (m.damage * modifierValue) / 100;

            if (targets != null)
            {
                Vector3 lookDir = new Vector3(targets[0].transform.position.x, gameObject.transform.position.y, targets[0].transform.position.z);
                gameObject.transform.LookAt(lookDir);
            }

            Vector3 pos = gameObject.transform.position;

            //animation for boss only atm
            if (GetComponent<PersonaController>().animator != null)
            {
                GetComponent<PersonaController>().animator.SetTrigger("fireball");
                SkinnedMeshRenderer modelMesh = gameObject.transform.GetChild(0).GetComponent<SkinnedMeshRenderer>();
                pos = modelMesh.bounds.center;
            }

            GameObject projectile = Instantiate(modifiedMove.projectile, pos, gameObject.transform.rotation);
            projectile.GetComponent<ProjectileController>().Initialize(modifiedMove, gameObject);

            cd = Time.time + m.cooldown;

            return true;
        }

        return false;
    }

    private bool CheckLineOfSight(GameObject target)
    {
        LayerMask arenaLayer = (1 << 11);
        if (Physics.Linecast(gameObject.transform.position, target.transform.position, arenaLayer))
        {
            return false;
        }

        return true;
    }
}
