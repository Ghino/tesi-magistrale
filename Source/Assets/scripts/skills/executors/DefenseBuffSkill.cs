﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//increase the defense of the target
public class DefenseBuffSkill : SkillExecutor
{
    private GameObject particles;

    public override void Initialize(Move m)
    {
        this.m = m;
        cd = 0;
        particles = Instantiate(m.effectParticle, gameObject.transform.position, Quaternion.identity, gameObject.transform);
    }

    public override void Reset()
    {
        StopAllCoroutines();
        base.Reset();
        particles.GetComponent<ParticleSystem>().Stop();
    }

    public override ExecutorErrorCodes CanExecute(List<GameObject> targets)
    {
        if (gameObject.GetComponent<PersonaController>().isCasting)
        {
            return ExecutorErrorCodes.Cast;
        }

        if (Time.time >= cd)
        {
            return ExecutorErrorCodes.OK;
        }
        else
        {
            return ExecutorErrorCodes.CD;
        }
    }

    public override bool ExecuteSkill(List<GameObject> targets)
    {
        if (CanExecute(targets).Equals(ExecutorErrorCodes.OK))
        {
            targets[0].GetComponent<PersonaController>().statsModifiers[m.affectedStat] = m.BDValue;

            particles.GetComponent<ParticleSystem>().Play();

            StartCoroutine(ApplyBuffDebuff(m, targets[0]));

            cd = Time.time + m.cooldown;

            return true;
        }

        return false;
    }

    private IEnumerator ApplyBuffDebuff(Move m, GameObject target)
    {
        yield return new WaitForSeconds(m.BDDuration);

        particles.GetComponent<ParticleSystem>().Stop();

        //remove buff/debuff
        target.GetComponent<PersonaController>().statsModifiers[m.affectedStat] = 0;
    }
}
