﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//standard melee attack
public class MeleeSlashSkill : SkillExecutor
{
    public override void Initialize(Move m)
    {
        this.m = m;
        cd = 0;
    }

    public override ExecutorErrorCodes CanExecute(List<GameObject> targets)
    {
        if (gameObject.GetComponent<PersonaController>().isCasting)
        {
            return ExecutorErrorCodes.Cast;
        }

        if (Time.time >= cd)
        {
            return ExecutorErrorCodes.OK;
        }
        else
        {
            return ExecutorErrorCodes.CD;
        }
    }

    public override bool ExecuteSkill(List<GameObject> targets)
    {
        if (CanExecute(targets).Equals(ExecutorErrorCodes.OK))
        {
            Move modifiedMove = ScriptableObject.CreateInstance<Move>();
            modifiedMove.Initialize(m);
            float modifierValue = gameObject.GetComponent<PersonaController>().statsModifiers[AffectedStat.Attack];
            modifiedMove.damage = m.damage + (m.damage * modifierValue) / 100;

            if (targets != null)
            {
                Vector3 lookDir = new Vector3(targets[0].transform.position.x, gameObject.transform.position.y, targets[0].transform.position.z);
                gameObject.transform.LookAt(lookDir);
            }

            gameObject.GetComponent<MeleeController>().Attack(modifiedMove);
            //animation for boss only atm
            if (GetComponent<PersonaController>().animator != null)
            {
                GetComponent<PersonaController>().animator.SetTrigger("slash");
            }

            cd = Time.time + m.cooldown;

            return true;
        }

        return false;
    }
}
