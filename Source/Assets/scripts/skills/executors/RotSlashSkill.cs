﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//melee aoe
public class RotSlashSkill : SkillExecutor
{
    public override void Initialize(Move m)
    {
        this.m = m;
        cd = 0;
    }

    public override ExecutorErrorCodes CanExecute(List<GameObject> targets)
    {
        if (gameObject.GetComponent<PersonaController>().isCasting)
        {
            return ExecutorErrorCodes.Cast;
        }

        if (Time.time >= cd)
        {
            return ExecutorErrorCodes.OK;
        }
        else
        {
            return ExecutorErrorCodes.CD;
        }
    }

    public override bool ExecuteSkill(List<GameObject> targets)
    {
        if (CanExecute(targets).Equals(ExecutorErrorCodes.OK))
        {
            Move modifiedMove = ScriptableObject.CreateInstance<Move>();
            modifiedMove.Initialize(m);
            float modifierValue = gameObject.GetComponent<PersonaController>().statsModifiers[AffectedStat.Attack];
            modifiedMove.damage = m.damage + (m.damage * modifierValue) / 100;

            gameObject.GetComponent<MeleeController>().RotAttack(modifiedMove);
            //animation for boss only atm
            if (GetComponent<PersonaController>().animator != null)
            {
                GetComponent<PersonaController>().animator.SetTrigger("rotSlash");
            }

            cd = Time.time + m.cooldown;

            return true;
        }

        return false;
    }
}
