﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//aoe force skill
public class AOEForceSkill : SkillExecutor
{
    private GameObject particles;
    private GameObject patternToActivate;
    private Coroutine instance = null;

    public override void Initialize(Move m)
    {
        this.m = m;
        cd = 0;
        particles = Instantiate(m.castParticle, gameObject.transform.position, Quaternion.identity, gameObject.transform);
    }

    public override void Reset()
    {
        if (instance != null)
            StopCoroutine(instance);
        particles.GetComponent<ParticleSystem>().Stop();
        Destroy(patternToActivate);
        base.Reset();
    }

    public override ExecutorErrorCodes CanExecute(List<GameObject> targets)
    {
        if (gameObject.GetComponent<PersonaController>().isCasting)
        {
            return ExecutorErrorCodes.Cast;
        }

        if (Time.time >= cd)
        {
            return ExecutorErrorCodes.OK;
        }
        else
        {
            return ExecutorErrorCodes.CD;
        }
    }

    public override bool ExecuteSkill(List<GameObject> targets)
    {
        if (CanExecute(targets).Equals(ExecutorErrorCodes.OK))
        {
            Destroy(patternToActivate);

            Move modifiedMove = ScriptableObject.CreateInstance<Move>();
            modifiedMove.Initialize(m);
            float modifierValue = gameObject.GetComponent<PersonaController>().statsModifiers[AffectedStat.Attack];
            modifiedMove.damage = m.damage + (m.damage * modifierValue) / 100;
            m = modifiedMove;

            Vector3 groundPos = new Vector3(gameObject.transform.position.x, gameObject.GetComponent<PersonaController>().groundPosY, gameObject.transform.position.z);
            GameObject pattern = Instantiate(m.attackPattern, groundPos, gameObject.transform.rotation);

            patternToActivate = pattern;

            //animation for boss only atm
            if (GetComponent<PersonaController>().animator != null)
            {
                GetComponent<PersonaController>().animator.SetTrigger("cast");
            }

            instance = StartCoroutine(CastSkill(null));

            return true;
        }

        return false;
    }

    private IEnumerator CastSkill(List<GameObject> targets)
    {
        gameObject.GetComponent<PersonaController>().SetIsCasting(true);

        particles.GetComponent<ParticleSystem>().Play();

        yield return new WaitForSeconds(m.castTime);

        particles.GetComponent<ParticleSystem>().Stop();

        gameObject.GetComponent<PersonaController>().SetIsCasting(false);

        //animation for boss only atm
        if (GetComponent<PersonaController>().animator != null)
        {
            GetComponent<PersonaController>().animator.SetTrigger("noCast");
        }

        //this check is to avoid errors during the reset of the environment since the stop coroutine doesn't interrupt the coroutine after passing the yield statement
        if (patternToActivate != null)
        {
            //in this case only one element of the pattern
            List<GameObject> charactersInside = patternToActivate.transform.GetChild(0).gameObject.GetComponent<PatternMarker>().GetAffectedCharacters();
            for (int y = charactersInside.Count - 1; y >= 0; y--)
            {
                if (CheckLineOfSight(charactersInside[y]))
                {
                    charactersInside[y].GetComponent<PersonaController>().AddHit(m.damage, m.moveName);
                    //force equals to half the dmg, and applied only if the character is still alive
                    if (charactersInside[y].activeSelf)
                    {
                        Vector3 force = (charactersInside[y].transform.position - gameObject.transform.position).normalized * (m.damage / 2);
                        force.y = 0;
                        charactersInside[y].GetComponent<PersonaController>().PrepareToApplyForce(0.3f);
                        charactersInside[y].GetComponent<Rigidbody>().AddForce(force, ForceMode.VelocityChange);
                    }
                }
            }

            patternToActivate.transform.GetChild(0).gameObject.GetComponent<PatternMarker>().Trigger(null);

            Destroy(patternToActivate);

            cd = Time.time + m.cooldown;
        }
    }

    private bool CheckLineOfSight(GameObject target)
    {
        LayerMask arenaLayer = (1 << 11);
        if (Physics.Linecast(gameObject.transform.position, target.transform.position, arenaLayer))
        {
            return false;
        }

        return true;
    }
}
