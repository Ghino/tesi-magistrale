﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AOEPillars : SkillExecutor
{
    private GameObject particles;
    private GameObject patternToActivate;
    private Coroutine instance = null;
    private bool activated = false;
    private GameObject arenaCenter;

    public override void Initialize(Move m)
    {
        this.m = m;
        cd = 0;
        particles = Instantiate(m.castParticle, gameObject.transform.position, Quaternion.identity, gameObject.transform);

        //trovare il punto di instanziazione corretto (centro arena)
        arenaCenter = gameObject.GetComponent<BossAgent>().arenaCenter;
    }

    public override void Reset()
    {
        if (instance != null)
            StopCoroutine(instance);
        particles.GetComponent<ParticleSystem>().Stop();
        Destroy(patternToActivate);
        base.Reset();
        activated = false;
    }

    public override ExecutorErrorCodes CanExecute(List<GameObject> targets)
    {
        if (gameObject.GetComponent<PersonaController>().isCasting)
        {
            return ExecutorErrorCodes.Cast;
        }

        if (activated)
        {
            return ExecutorErrorCodes.OneTime;
        }
        else
        {
            return ExecutorErrorCodes.OK;
        }
    }

    public override bool ExecuteSkill(List<GameObject> targets)
    {
        if (CanExecute(targets).Equals(ExecutorErrorCodes.OK))
        {
            Destroy(patternToActivate);

            //move the boss at the center of the arena
            Vector3 transitionPos = new Vector3(arenaCenter.transform.position.x, arenaCenter.transform.position.y +
                (gameObject.GetComponent<CapsuleCollider>().height / 2) * gameObject.transform.localScale.y, arenaCenter.transform.position.z);

            transform.position = transitionPos;

            //posizione centro arena
            GameObject pattern = Instantiate(m.attackPattern, arenaCenter.transform.position, Quaternion.identity);

            patternToActivate = pattern;

            instance = StartCoroutine(CastSkill(null));

            return true;
        }

        return false;
    }

    private IEnumerator CastSkill(List<GameObject> targets)
    {
        gameObject.GetComponent<PersonaController>().SetIsCasting(true);

        particles.GetComponent<ParticleSystem>().Play();

        yield return new WaitForSeconds(m.castTime);

        particles.GetComponent<ParticleSystem>().Stop();

        gameObject.GetComponent<PersonaController>().SetIsCasting(false);

        if (patternToActivate != null)
        {
            for (int i = 0; i < patternToActivate.transform.childCount; i++)
            {
                List<GameObject> charactersInside = patternToActivate.transform.GetChild(i).gameObject.GetComponent<PatternMarker>().GetAffectedCharacters();
                for (int y = charactersInside.Count - 1; y >= 0; y--)
                {
                    charactersInside[y].GetComponent<PersonaController>().AddHit(m.damage, m.moveName);
                    //then they will be moved near the pillar
                    LayerMask arenaLayer = (1 << 11);
                    if (NavMesh.SamplePosition(patternToActivate.transform.GetChild(i).position, out NavMeshHit hit, 5, arenaLayer))
                    {
                        Vector3 pos = new Vector3(hit.position.x, charactersInside[y].GetComponent<CapsuleCollider>().height / 2, hit.position.z);
                        charactersInside[y].GetComponent<Rigidbody>().MovePosition(pos);
                    }
                }

                patternToActivate.transform.GetChild(i).gameObject.GetComponent<PatternMarker>().Trigger(arenaCenter);
            }

            Destroy(patternToActivate);

            activated = true;
        }
    }
}
