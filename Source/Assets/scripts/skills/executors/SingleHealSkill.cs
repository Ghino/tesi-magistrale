﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//healing skill single target
public class SingleHealSkill : SkillExecutor
{
    private GameObject particles;

    public override void Initialize(Move m)
    {
        this.m = m;
        cd = 0;
        particles = Instantiate(m.castParticle, gameObject.transform.position, Quaternion.identity, gameObject.transform);
    }

    public override void Reset()
    {
        StopAllCoroutines();
        base.Reset();
        particles.GetComponent<ParticleSystem>().Stop();
    }

    public override ExecutorErrorCodes CanExecute(List<GameObject> targets)
    {
        if (gameObject.GetComponent<PersonaController>().isCasting)
        {
            return ExecutorErrorCodes.Cast;
        }

        if (Time.time >= cd)
        {
            if(targets == null)
            {
                return ExecutorErrorCodes.OK;
            }
            if(Vector3.Distance(gameObject.transform.position, targets[0].transform.position) < m.maxRange)
            {
                if (CheckLineOfSight(targets[0]))
                {
                    return ExecutorErrorCodes.OK;
                }
                else
                {
                    return ExecutorErrorCodes.Sight;
                }
            }
            else
            {
                return ExecutorErrorCodes.Distance;
            }
        }
        else
        {
            return ExecutorErrorCodes.CD;
        }
    }

    public override bool ExecuteSkill(List<GameObject> targets)
    {
        if (CanExecute(targets).Equals(ExecutorErrorCodes.OK))
        {
            StartCoroutine(CastSkill(targets));

            return true;
        }

        return false;
    }

    private bool CheckLineOfSight(GameObject target)
    {
        LayerMask arenaLayer = (1 << 11);
        if (Physics.Linecast(gameObject.transform.position, target.transform.position, arenaLayer))
        {
            return false;
        }

        return true;
    }

    private IEnumerator CastSkill(List<GameObject> targets)
    {
        gameObject.GetComponent<PersonaController>().SetIsCasting(true);

        if (targets != null)
        {
            Vector3 lookDir = new Vector3(targets[0].transform.position.x, gameObject.transform.position.y, targets[0].transform.position.z);
            gameObject.transform.LookAt(lookDir);
        }

        particles.GetComponent<ParticleSystem>().Play();

        yield return new WaitForSeconds(m.castTime);

        particles.GetComponent<ParticleSystem>().Stop();

        gameObject.GetComponent<PersonaController>().SetIsCasting(false);

        if (CanExecute(targets).Equals(ExecutorErrorCodes.OK))
        {
            if (targets[0].activeSelf)
            {
                Vector3 nextObj = targets[0].gameObject.transform.position;
                Vector3 lookDir = new Vector3(nextObj.x, gameObject.transform.position.y, nextObj.z);
                gameObject.transform.LookAt(lookDir);

                targets[0].GetComponent<PersonaController>().AddHeal(m.healPotency, m.healParticle);
                cd = Time.time + m.cooldown;
            }
        }
    }
}
