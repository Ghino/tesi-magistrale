﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//skill to create an aoe with a certain pattern
public class AOEPatternSkill : SkillExecutor
{
    private GameObject particles;
    private GameObject patternToActivate;
    private Coroutine instance = null;

    public override void Initialize(Move m)
    {
        this.m = m;
        cd = 0;
        particles = Instantiate(m.castParticle, gameObject.transform.position, Quaternion.identity, gameObject.transform);
    }

    public override void Reset()
    {
        if (instance != null)
            StopCoroutine(instance);
        particles.GetComponent<ParticleSystem>().Stop();
        Destroy(patternToActivate);
        base.Reset();
    }

    public override ExecutorErrorCodes CanExecute(List<GameObject> targets)
    {
        if (gameObject.GetComponent<PersonaController>().isCasting)
        {
            return ExecutorErrorCodes.Cast;
        }

        if (Time.time >= cd)
        {
            return ExecutorErrorCodes.OK;
        }
        else
        {
            return ExecutorErrorCodes.CD;
        }
    }

    public override bool ExecuteSkill(List<GameObject> targets)
    {
        if (CanExecute(targets).Equals(ExecutorErrorCodes.OK))
        {
            Destroy(patternToActivate);

            Move modifiedMove = ScriptableObject.CreateInstance<Move>();
            modifiedMove.Initialize(m);
            float modifierValue = gameObject.GetComponent<PersonaController>().statsModifiers[AffectedStat.Attack];
            modifiedMove.damage = m.damage + (m.damage * modifierValue) / 100;
            m = modifiedMove;

            Vector3 groundPos = new Vector3(gameObject.transform.position.x, gameObject.GetComponent<PersonaController>().groundPosY, gameObject.transform.position.z);
            GameObject pattern = Instantiate(m.attackPattern, groundPos, gameObject.transform.rotation);

            patternToActivate = pattern;

            //animation for boss only atm
            if (GetComponent<PersonaController>().animator != null)
            {
                GetComponent<PersonaController>().animator.SetTrigger("cast");
            }

            instance = StartCoroutine(CastSkill(null));

            return true;
        }

        return false;
    }

    private IEnumerator CastSkill(List<GameObject> targets)
    {
        gameObject.GetComponent<PersonaController>().SetIsCasting(true);

        particles.GetComponent<ParticleSystem>().Play();

        yield return new WaitForSeconds(m.castTime);

        particles.GetComponent<ParticleSystem>().Stop();

        gameObject.GetComponent<PersonaController>().SetIsCasting(false);

        //animation for boss only atm
        if (GetComponent<PersonaController>().animator != null)
        {
            GetComponent<PersonaController>().animator.SetTrigger("noCast");
        }

        if (patternToActivate != null)
        {
            for (int i = 0; i < patternToActivate.transform.childCount; i++)
            {
                List<GameObject> characterInside = patternToActivate.transform.GetChild(i).gameObject.GetComponent<PatternMarker>().GetAffectedCharacters();
                foreach (GameObject character in characterInside)
                {
                    character.GetComponent<PersonaController>().AddHit(m.damage, m.moveName);
                }
                patternToActivate.transform.GetChild(i).gameObject.GetComponent<PatternMarker>().Trigger(null);
            }

            Destroy(patternToActivate);

            cd = Time.time + m.cooldown;
        }
    }
}
