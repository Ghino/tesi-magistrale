﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//heal aoe
public class MultipleHealSkill : SkillExecutor
{
    private GameObject particles;

    public override void Initialize(Move m)
    {
        this.m = m;
        cd = 0;
        particles = Instantiate(m.castParticle, gameObject.transform.position, Quaternion.identity, gameObject.transform);
    }

    public override void Reset()
    {
        StopAllCoroutines();
        base.Reset();
        particles.GetComponent<ParticleSystem>().Stop();
    }

    public override ExecutorErrorCodes CanExecute(List<GameObject> targets)
    {
        if (gameObject.GetComponent<PersonaController>().isCasting)
        {
            return ExecutorErrorCodes.Cast;
        }

        if (Time.time >= cd)
        {
            return ExecutorErrorCodes.OK;
        }
        else
        {
            return ExecutorErrorCodes.CD;
        }
    }

    public override bool ExecuteSkill(List<GameObject> targets)
    {
        if (CanExecute(targets).Equals(ExecutorErrorCodes.OK))
        {
            StartCoroutine(CastSkill(targets));

            return true;
        }

        return false;
    }

    private IEnumerator CastSkill(List<GameObject> targets)
    {
        gameObject.GetComponent<PersonaController>().SetIsCasting(true);

        particles.GetComponent<ParticleSystem>().Play();

        yield return new WaitForSeconds(m.castTime);

        particles.GetComponent<ParticleSystem>().Stop();

        gameObject.GetComponent<PersonaController>().SetIsCasting(false);

        foreach (GameObject target in targets)
        {
            if (target.activeSelf)
            {
                if (Vector3.Distance(gameObject.transform.position, target.transform.position) < m.maxRange)
                {
                    target.GetComponent<PersonaController>().AddHeal(m.healPotency, m.healParticle);
                }
            }
        }

        cd = Time.time + m.cooldown;
    }
}
