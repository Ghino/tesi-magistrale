﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//jump away from target
public class JumpAwaySkill : SkillExecutor
{
    private bool activated = false;
    private Vector3 startingPos;

    public override void Initialize(Move m)
    {
        this.m = m;
        cd = 0;
    }

    public override void Reset()
    {
        base.Reset();
        activated = false;
    }

    private void FixedUpdate()
    {
        if (activated && Vector3.Distance(startingPos, gameObject.transform.position) >= m.distance)
        {
            gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            activated = false;
        }
    }

    public override ExecutorErrorCodes CanExecute(List<GameObject> targets)
    {
        if (gameObject.GetComponent<PersonaController>().isCasting)
        {
            return ExecutorErrorCodes.Cast;
        }

        if (Time.time >= cd)
        {
            if (targets == null)
            {
                return ExecutorErrorCodes.OK;
            }

            if (Vector3.Distance(gameObject.transform.position, targets[0].transform.position) < 3)
            {
                return ExecutorErrorCodes.OK;
            }
            else
            {
                return ExecutorErrorCodes.Distance;
            }
        }
        else
        {
            return ExecutorErrorCodes.CD;
        }
    }

    public override bool ExecuteSkill(List<GameObject> targets)
    {
        if (CanExecute(targets).Equals(ExecutorErrorCodes.OK))
        {
            Vector3 lookDir = new Vector3(targets[0].transform.position.x, gameObject.transform.position.y, targets[0].transform.position.z);
            gameObject.transform.LookAt(lookDir);
            gameObject.GetComponent<PersonaController>().PrepareToApplyForce(0.2f);
            gameObject.GetComponent<Rigidbody>().AddForce(-gameObject.transform.forward * m.movPower * 10, ForceMode.VelocityChange);

            cd = Time.time + m.cooldown;
            activated = true;
            startingPos = gameObject.transform.position;

            return true;
        }

        return false;
    }
}
