﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectDeathTimer : MonoBehaviour
{
    public float secondsToLive = 1f;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            gameObject.transform.GetChild(i).gameObject.GetComponent<ParticleSystem>().Play();
        }
        Destroy(gameObject, secondsToLive);
    }
}
