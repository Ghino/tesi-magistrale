﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour
{
    public GameObject muzzleEffect;
    public GameObject hitEffect;
    private float speed = 0;
    private float dmg = 0;
    private float maxDistance = 0;
    private Vector3 origin;
    private GameObject parent;
    private bool isBoss = false;

    void Start()
    {
        
    }

    public void Initialize(Move move, GameObject parent)
    {
        this.speed = move.attackSpeed;
        this.dmg = move.damage;
        this.maxDistance = move.maxRange;
        //per ora solo origine da giocatore
        this.origin = parent.transform.position;
        this.parent = parent;
        if (parent.tag.Equals("Boss"))
        {
            isBoss = true;
        }

        GameObject muzzle = Instantiate(muzzleEffect, transform.position, transform.rotation);
        Destroy(muzzle, muzzle.transform.GetChild(0).gameObject.GetComponent<ParticleSystem>().main.duration);
    }

    
    private void FixedUpdate()
    {
        if (Vector3.Distance(origin, gameObject.transform.position) > maxDistance)
        {
            Destroy(gameObject);
        }
        
        float moveDistance = speed * Time.fixedDeltaTime;

        // move bullet
        transform.Translate(Vector3.forward * moveDistance);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (isBoss)
        {
            if (other.gameObject.tag.Equals("AIPlayer"))
            {
                other.gameObject.GetComponent<PersonaController>().AddHit(dmg, "Projectile");
                GameObject hit = Instantiate(hitEffect, transform.position, Quaternion.identity);
                Destroy(hit, hit.transform.GetChild(0).gameObject.GetComponent<ParticleSystem>().main.duration);
                Destroy(gameObject);
            }
        }
        else
        {
            if (other.gameObject.tag.Equals("Boss"))
            {
                other.gameObject.GetComponent<PersonaController>().AddHit(dmg, null);
                GameObject hit = Instantiate(hitEffect, transform.position, Quaternion.identity);
                Destroy(hit, hit.transform.GetChild(0).gameObject.GetComponent<ParticleSystem>().main.duration);
                Destroy(gameObject);
            }
        }

        if (other.gameObject.tag.Equals("Arena"))
        {
            GameObject hit = Instantiate(hitEffect, transform.position, Quaternion.identity);
            Destroy(hit, hit.transform.GetChild(0).gameObject.GetComponent<ParticleSystem>().main.duration);
            Destroy(gameObject);
        }
    }
}
