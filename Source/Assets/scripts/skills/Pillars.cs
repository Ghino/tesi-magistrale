﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Pillars : MonoBehaviour
{
    public float dmgOnContact = 20f;
    private Vector3 startPos;
    private Vector3 targetPos;
    private float t = 0;

    // Start is called before the first frame update
    void Start()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y - 5, transform.position.z);
        startPos = transform.position;
        targetPos = new Vector3(transform.position.x, transform.position.y + 2, transform.position.z);
    }

    private void FixedUpdate()
    {
        t += Time.fixedDeltaTime * 5;
        transform.position = Vector3.Lerp(startPos, targetPos, t);
    }

    //if players collide with the pillars they take dmg
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("AIPlayer"))
        {
            collision.gameObject.GetComponent<PersonaController>().AddHit(dmgOnContact, null);
        }
    }
}
