﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//script to expand the sphere representing the effect of the aoe force
public class ForceSphereVFX : MonoBehaviour
{
    public float maxExpansion = 60;
    public float expansionSpeed = 10;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void FixedUpdate()
    {
        if(gameObject.transform.localScale.x < maxExpansion)
        {
            gameObject.transform.localScale = Vector3.Lerp(gameObject.transform.localScale, 
                new Vector3(maxExpansion + 1, maxExpansion + 1, maxExpansion + 1), Time.fixedDeltaTime * expansionSpeed);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
