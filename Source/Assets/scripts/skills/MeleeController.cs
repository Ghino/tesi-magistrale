﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeController : MonoBehaviour
{
    private bool isBoss = false;
    private GameObject joint;
    private string moveName;
    private float dmg = 0;
    private bool isAttacking = false;
    private Transform tip;
    private float hitSpeed = 0;
    private List<int> hitChecks = new List<int>();

    //values to control the rotation of the joint
    private bool down = false;
    private bool up = false;
    private Vector3 rotUp = new Vector3(-40, -20, 0);
    private Vector3 rotDown = new Vector3(40, 20, 0);
    private Vector3 rotSpin = new Vector3(0, 20, 0);
    private float rotLeft = 360;

    private bool rotAttack = false;

    void Start()
    {
        if (gameObject.tag.Equals("Boss"))
        {
            isBoss = true;
        }
        joint = gameObject.transform.GetChild(0).gameObject;
        tip = gameObject.transform.GetChild(0).GetChild(0).GetChild(0);
    }

    //normal attack
    public void Attack(Move m)
    {
        this.moveName = m.moveName;
        this.dmg = m.damage;
        this.hitSpeed = m.attackSpeed;

        isAttacking = true;
        hitChecks.Clear();

        down = true;
    }

    //aoe melee
    public void RotAttack(Move m)
    {
        this.moveName = m.moveName;
        this.dmg = m.damage;
        this.hitSpeed = m.attackSpeed;

        hitChecks.Clear();
        isAttacking = true;
        gameObject.GetComponent<PersonaController>().SetIsCasting(true);
        joint.transform.localRotation = Quaternion.Euler(rotSpin);
        rotAttack = true;
    }

    //the sword of the character is controlled by rotating the joint attached
    private void FixedUpdate()
    {
        if (isAttacking)
        {
            //aoe
            if (rotAttack)
            {
                float rotation = hitSpeed * Time.fixedDeltaTime * 100;
                if (rotLeft > rotation)
                {
                    rotLeft -= rotation;
                }
                else
                {
                    rotation = rotLeft;
                    rotLeft = 0;
                }

                transform.Rotate(0, rotation, 0);

                if(rotLeft == 0)
                {
                    joint.transform.localRotation = Quaternion.Euler(rotUp);
                    isAttacking = false;
                    rotLeft = 360;
                    rotAttack = false;
                    gameObject.GetComponent<PersonaController>().SetIsCasting(false);
                }
            }
            else //standard
            {
                if (down)
                {
                    joint.transform.Rotate(rotDown.normalized * hitSpeed * Time.fixedDeltaTime * 100, Space.Self);
                    float yDiff = Mathf.DeltaAngle(joint.transform.localEulerAngles.y, rotDown.y);
                    float xDiff = Mathf.DeltaAngle(joint.transform.localEulerAngles.x, rotDown.x);
                    if (xDiff < 1 && yDiff < 1)
                    {
                        down = false;
                        up = true;
                    }
                }

                if (up)
                {
                    joint.transform.localRotation = Quaternion.Euler(rotUp);
                    up = false;
                    isAttacking = false;
                }
            }

            //hit checks, mask to exclude party members
            LayerMask mask;

            if (gameObject.tag.Equals("Boss"))
            {
                mask = (1 << 10);
            }
            else
            {
                mask = (1 << 9);
            }

            mask = ~mask;

            if (Physics.Linecast(joint.transform.position, tip.position, out RaycastHit hit, mask))
            {
                if (!hitChecks.Contains(hit.collider.gameObject.GetInstanceID()))
                {
                    if (isBoss)
                    {
                        if (hit.collider.gameObject.tag.Equals("AIPlayer"))
                        {
                            hit.collider.gameObject.GetComponent<PersonaController>().AddHit(dmg, "Slash");
                        }
                    }
                    else
                    {
                        if (hit.collider.gameObject.tag.Equals("Boss"))
                        {
                            hit.collider.gameObject.GetComponent<PersonaController>().AddHit(dmg, null);
                        }
                    }

                    hitChecks.Add(hit.collider.gameObject.GetInstanceID());
                }
            }

        }

    }

    public float GetMeleeRange()
    {
        float distance = Vector3.Distance(joint.transform.position, tip.position);

        return distance;
    }
}
