﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//abstract class for the logic behind every single skill in the game
public abstract class SkillExecutor : MonoBehaviour
{
    protected Move m;
    protected float cd;

    //initialize the skill by coping the necessary parameters from the scriptable object Move
    public virtual void Initialize(Move m)
    {
        this.m = m;
        cd = 0;
    }

    //reset the skill
    public virtual void Reset()
    {
        cd = 0;
    }
    //check if skill can be executed, return a error code representing which check hasn't passed
    //if called with null as parameter, check only cd and if the character is casting another skill
    public abstract ExecutorErrorCodes CanExecute(List<GameObject> targets);
    //executes the skill
    public abstract bool ExecuteSkill(List<GameObject> targets);

    //immidiately stops the cast of the skill
    public void StopCast()
    {
        StopAllCoroutines();
    }
}

//codes for skill execution, in order: ok if can be executed, error if cd is still up, error if player is casting another skill
//error if the skill distance is not respected, error if the character can't see the target or if the skill is one time activation and it's already executed
public enum ExecutorErrorCodes { OK, CD, Cast, Distance, Sight, OneTime }
