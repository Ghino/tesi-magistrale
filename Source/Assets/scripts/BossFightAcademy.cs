﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;
using System.IO;

public class BossFightAcademy : Academy
{
    public Vector2 rowsXcolumns = new Vector2();
    public GameObject fightArea;
    public GameObject objectToInstantiate;
    public GameObject UI;
    public bool logData = false;

    private List<List<GameObject>> agentsPerInstance = new List<List<GameObject>>();

    //log info
    private int run = 1;
    private Dictionary<int, CharacterCategory> categoryIndex = new Dictionary<int, CharacterCategory>();
    private Dictionary<int, List<float>> winLifes = new Dictionary<int, List<float>>();
    private Dictionary<int, List<float>> defeatLifes = new Dictionary<int, List<float>>();
    private List<float> allDurations = new List<float>();

    //initialize the ambient
    //creates a matrix of instances
    public override void InitializeAcademy()
    {
        GameObject tmp = Instantiate(fightArea);
        float distanceFromInstances = Vector3.Distance(tmp.GetComponent<MeshCollider>().bounds.min, tmp.GetComponent<MeshCollider>().bounds.max) * 3;
        DestroyImmediate(tmp);
        List<GameObject> instances = new List<GameObject>();
        GameObject parentOfAreas = new GameObject("parent");
        parentOfAreas.transform.position = Vector3.zero;

        for (int i = 0; i < rowsXcolumns.x; i++)
        {
            for(int j = 0; j < rowsXcolumns.y; j++)
            {
                Vector3 pos = new Vector3(i * distanceFromInstances, 0, j * distanceFromInstances);
                GameObject instance = Instantiate(objectToInstantiate, pos, Quaternion.identity, parentOfAreas.transform);
                instances.Add(instance);

                //links the UI of the first instance
                if(i == 0 && j == 0)
                {
                    List<GameObject> characters = new List<GameObject>();
                    for (int a = 0; a < instance.transform.childCount; a++)
                    {
                        GameObject child = instance.transform.GetChild(a).gameObject;
                        if(child.activeSelf && child.GetComponent<PersonaController>() != null)
                        {
                            characters.Add(child);
                            child.GetComponent<PersonaController>().logWindow = UI.transform.GetChild(1).gameObject;
                        }
                    }

                    //log info of the run
                    string path = Application.dataPath + "/Log.txt";

                    if (!File.Exists(path))
                    {
                        File.WriteAllText(path, "Results:");
                    }

                    //log preparation
                    if (logData)
                    {
                        File.AppendAllText(path, "\n\n\nNew Execution with the following characters: ");
                        int index = 0;
                        foreach (GameObject character in characters)
                        {
                            File.AppendAllText(path, character.GetComponent<PersonaController>().characterCategory.ToString() + " ");
                            winLifes.Add(index, new List<float>());
                            defeatLifes.Add(index, new List<float>());
                            categoryIndex.Add(index, character.GetComponent<PersonaController>().characterCategory);
                            index++;
                        }

                        //skills
                        File.AppendAllText(path, "\nSKILL NAME\t\t\tCD\t\t\tDMG\t\t\tBUFF%\t\t\tDURATION");
                        foreach (GameObject character in characters)
                        {
                            if (character.GetComponent<PersonaController>().characterCategory == CharacterCategory.Boss)
                            {
                                foreach (Move move in character.GetComponent<BossAgent>().bossMoves)
                                {
                                    File.AppendAllText(path, "\n" + move.name + "\t\t\t" + move.cooldown + "\t\t\t" + move.damage + "\t\t\t" + move.BDValue + "\t\t\t" + move.BDDuration);
                                }
                            }
                            else
                            {
                                foreach (Move move in character.GetComponent<PlayerAI>().moves)
                                {
                                    File.AppendAllText(path, "\n" + move.name + "\t\t\t" + move.cooldown + "\t\t\t" + move.damage + "\t\t\t" + move.BDValue + "\t\t\t" + move.BDDuration);
                                }
                            }
                        }

                        File.AppendAllText(path, "\n");
                    }

                    List<GameObject> uiPlayers = new List<GameObject>();
                    for (int a = 0; a < UI.transform.GetChild(0).childCount; a++)
                    {
                        uiPlayers.Add(UI.transform.GetChild(0).GetChild(a).gameObject);
                    }

                    for (int a = 0; a < uiPlayers.Count; a++)
                    {
                        if (a < characters.Count)
                        {
                            uiPlayers[a].GetComponent<GuiUpdater>().character = characters[a];
                        }
                        else
                        {
                            uiPlayers[a].SetActive(false);
                        }
                    }
                }
            }
        }
        
        //randomize the positions of the characters for each instance
        foreach(GameObject instance in instances)
        {
            List<GameObject> agents= new List<GameObject>();
            for(int i = 0; i < instance.transform.childCount; i++)
            {
                GameObject child = instance.transform.GetChild(i).gameObject;
                if(child.tag.Equals("Boss") || child.tag.Equals("AIPlayer"))
                {
                    agents.Add(child);
                }
            }

            ShufflePos(agents);
            agentsPerInstance.Add(agents);
        }
    }

    //
    public override void AcademyReset()
    {
        //possibilità di gestire il reset degli agenti qui
    }

    //
    public override void AcademyStep()
    {
        
    }

    //shuffles the list
    private void ShufflePos(List<GameObject> agents)
    {
        List<Vector3> positions = new List<Vector3>();
        foreach(GameObject agent in agents)
        {
            positions.Add(agent.transform.position);
        }

        int n = positions.Count;
        while (n > 1)
        {
            n--;
            int k = Random.Range(0, n + 1);
            Vector3 value = positions[k];
            positions[k] = positions[n];
            positions[n] = value;
        }

        for(int i = 0; i < agents.Count; i++)
        {
            agents[i].transform.position = positions[i];
        }
    }

    public void AddLogInfo(string info, float duration, bool win, Dictionary<int, float> lifes)
    {
        if (!logData)
        {
            return;
        }

        string path = Application.dataPath + "/Log.txt";

        Debug.Log("Run num: " + run + ", duration: " + duration);

        //File.AppendAllText(path, "\nRun num: " + run + ", duration: " + duration);

        //File.AppendAllText(path, info);

        allDurations.Add(duration);

        if (win)
        {
            foreach(int character in lifes.Keys)
            {
                winLifes[character].Add(lifes[character]);
            }
        }
        else
        {
            foreach (int character in lifes.Keys)
            {
                defeatLifes[character].Add(lifes[character]);
            }
        }

        //at the 100th fight calculate the average of the results
        if (run == 100)
        {
            string average = "\n\nFinal Average:";
            //0 index of the boss, first character of each instance
            average = average + "\nNum of WIN: " + winLifes[0].Count + "/100";

            float tmpValue = 0;
            foreach (float life in winLifes[0])
            {
                tmpValue += life;
            }

            average = average + "\n\t Boss average life left: " + tmpValue / winLifes[0].Count;

            for(int i = 1; i < winLifes.Keys.Count; i++)
            {
                average = average + "\n\t " + categoryIndex[i].ToString() + " average life left: 0";
            }

            average = average + "\nNum of DEFEAT: " + defeatLifes[0].Count + "/100";

            average = average + "\n\t Boss average life left: 0";

            for (int i = 1; i < winLifes.Keys.Count; i++)
            {
                tmpValue = 0;
                foreach (float life in defeatLifes[i])
                {
                    tmpValue += life;
                }
                tmpValue /= defeatLifes[i].Count;

                average = average + "\n\t " + categoryIndex[i].ToString() + " average life left: " + tmpValue;
            }

            float averageDuration = 0;
            foreach (float value in allDurations)
            {
                averageDuration += value;
            }

            average = average + "\naverage duration: " + averageDuration / allDurations.Count + "\n\n";

            File.AppendAllText(path, average);

            #if UNITY_EDITOR
                        UnityEditor.EditorApplication.isPlaying = false;
            #else
                        Application.Quit();
            #endif
        }

        run++;
    }
}