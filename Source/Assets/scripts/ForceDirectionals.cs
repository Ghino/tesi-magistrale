﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceDirectionals : MonoBehaviour
{
    public GameObject directional;
    public float spawnEachSecond = 1f;
    private float timer = 0;
    // Start is called before the first frame update
    void Start()
    {
        timer = Time.time;
    }

    void FixedUpdate()
    {
        if(Time.time > timer)
        {
            timer = Time.time + spawnEachSecond;
            for(int i = 0; i < transform.childCount; i++)
            {
                GameObject arrow = Instantiate(directional, transform.position, Quaternion.identity, transform.GetChild(i));
                arrow.transform.SetParent(transform.GetChild(i), false);
                
                arrow.GetComponent<AtoBMov>().Initialize(transform.position, transform.GetChild(i).position);
            }
        }
    }
}
