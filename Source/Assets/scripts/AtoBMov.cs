﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtoBMov : MonoBehaviour
{
    private Vector3 startPos;
    private Vector3 endPos;
    public float speed = 3;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Initialize(Vector3 start, Vector3 end)
    {
        startPos = start;
        endPos = end;

        Vector3 targetDir = end - start;
        transform.GetChild(1).rotation = Quaternion.LookRotation(targetDir);
    }

    void FixedUpdate()
    {
        Vector3 direction = endPos - startPos;
        transform.Translate(direction * speed * Time.fixedDeltaTime);
        if (Vector3.Distance(transform.position, endPos) < 5)
        {
            Destroy(gameObject);
        }
    }
}
