﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEditor;
using Random = UnityEngine.Random;

public class PersonaController : MonoBehaviour
{
    //base
    public CharacterCategory characterCategory; 
    public float movementSpeed = 10;
    public float bossRotationSpeed = 10;
    public float stoppingDistance { get; private set; }

    //movement
    private Rigidbody rBody;
    private NavMeshAgent agent;
    private Vector3 resetPos;
    private Vector3 nextPos;
    public float groundPosY { get; private set; }
    public bool isForceApplied { get; private set; } = false;

    //stats
    public float health = 100;
    public float MaxHealth { get; private set; }
    private GameObject healParticle;
    public bool isCasting { get; private set; } = false;
    //all the stats modifiers (positive and negative ones)
    public Dictionary<AffectedStat, float> statsModifiers = new Dictionary<AffectedStat, float>();

    //dangers
    public bool InsideDangerZone { get; private set; } = false;
    public bool InsideBorder { get; private set; } = false;
    public bool IncomingProjectile { get; private set; } = false;
    public bool InsideForceZone { get; private set; } = false;
    private Collider dangerCollider;
    private Collider borderCollider;
    private Collider forceCollider;
    public  Vector3 dangerZonePos { get; private set; }
    public Vector3 bossProjectilePos { get; private set; }
    private KeyValuePair<float, float> dangerReactionTime = new KeyValuePair<float, float>(0.10f, 0.70f);
    private float borderTimer = 0;

    public Animator animator { get; private set; }

    public GameObject logWindow;

    // Start is called before the first frame update
    void Start()
    {
        resetPos = gameObject.transform.position;

        if (characterCategory != CharacterCategory.Boss)
        {
            agent = gameObject.GetComponent<NavMeshAgent>();
            //the width of the character (since their scale is 1, i'll get the radius of the capsule collider * 2)
            stoppingDistance = gameObject.GetComponent<CapsuleCollider>().radius * 2;
        }
        else //momentaneo solo per boss
        {
            animator = GetComponent<Animator>();
        }
        rBody = gameObject.GetComponent<Rigidbody>();

        MaxHealth = health;

        foreach(AffectedStat stat in Enum.GetValues(typeof(AffectedStat)))
        {
            statsModifiers.Add(stat, 0);
        }

        groundPosY = 1;
    }

    private void FixedUpdate()
    {
        //movement
        if (characterCategory != CharacterCategory.Boss)
        {
            //if the player needs to go melee, stop a bit before the boss position to take into account the boss size, his size and the melee range for attacks
            if (gameObject.GetComponent<MeleeController>() != null)
            {
                //considering a certain error of boss movement
                if (Vector3.Distance(gameObject.GetComponent<PlayerAI>().boss.transform.position, nextPos) <= 2)
                {
                    float distance = gameObject.GetComponent<MeleeController>().GetMeleeRange() / 2 + gameObject.GetComponent<CapsuleCollider>().radius +
                        (gameObject.GetComponent<PlayerAI>().boss.GetComponent<CapsuleCollider>().radius * gameObject.GetComponent<PlayerAI>().boss.transform.localScale.x);
                    if (Vector3.Distance(gameObject.transform.position, nextPos) <= distance)
                    {
                        agent.velocity = Vector3.zero;
                        agent.isStopped = true;
                    }
                }
            }
            else //else stop a bit before to take into account the other players size
            {
                if (Vector3.Distance(gameObject.transform.position, nextPos) < stoppingDistance)
                {
                    agent.velocity = Vector3.zero;
                    agent.isStopped = true;
                }
            }
        }
        else
        {
            //check fase 2
            if(health <= (MaxHealth * 50) / 100)
            {
                if (!gameObject.GetComponent<BossAgent>().pillarActivated)
                {
                    gameObject.GetComponent<BossAgent>().SecondPhase();
                }
            }
        }


        if (InsideDangerZone && dangerCollider == null)
        {
            InsideDangerZone = false;
        }

        if (InsideBorder && borderCollider == null)
        {
            InsideBorder = false;
        }
        if (InsideForceZone && forceCollider == null)
        {
            InsideForceZone = false;
        }
        else if(InsideBorder)
        {
            //if in border 15 dmg each second
            if(Time.time > borderTimer)
            {
                AddHit(15, "Lava");
                borderTimer = Time.time + 1;
            }
        }

        if (characterCategory != CharacterCategory.Boss && SeeIncomingProjectile())
        {
            //there is a possibility that the character dies right before starting the coroutine
            if (gameObject.activeSelf)
            {
                StartCoroutine(DangerReaction(Random.Range(dangerReactionTime.Key, dangerReactionTime.Value), 2));
            }
            
        }
        else
        {
            IncomingProjectile = false;
        }
    }

    //checks the position of the character, if he enters determinate zones
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("DangerZone"))
        {
            if (characterCategory != CharacterCategory.Boss)
            {
                StartCoroutine(DangerReaction(Random.Range(dangerReactionTime.Key, dangerReactionTime.Value), 1));
            }
            else
            {
                InsideDangerZone = true;
            }
            
            dangerZonePos = other.transform.position;
            dangerCollider = other;
        }
        if (other.gameObject.tag.Equals("DangerBorder"))
        {
            if (characterCategory != CharacterCategory.Boss)
            {
                StartCoroutine(DangerReaction(Random.Range(dangerReactionTime.Key, dangerReactionTime.Value), 0));
            }
            else
            {
                InsideBorder = true;
            }
            
            borderCollider = other;
            borderTimer = Time.time + 1;
        }

        if (other.gameObject.tag.Equals("ForceZone"))
        {
            if (characterCategory != CharacterCategory.Boss)
            {
                StartCoroutine(DangerReaction(Random.Range(dangerReactionTime.Key, dangerReactionTime.Value), 3));
            }
            else
            {
                InsideForceZone = true;
            }
            forceCollider = other;
        }
    }

    //checks the position of the character, if he is inside determinate zones
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag.Equals("DangerZone"))
        {
            if (characterCategory != CharacterCategory.Boss)
            {
                StartCoroutine(DangerReaction(Random.Range(dangerReactionTime.Key, dangerReactionTime.Value), 1));
            }
            else
            {
                InsideDangerZone = true;
            }
            dangerZonePos = other.transform.position;
            dangerCollider = other;
        }
        if (other.gameObject.tag.Equals("DangerBorder"))
        {
            if (characterCategory != CharacterCategory.Boss)
            {
                StartCoroutine(DangerReaction(Random.Range(dangerReactionTime.Key, dangerReactionTime.Value), 0));
            }
            else
            {
                InsideBorder = true;
            }
            borderCollider = other;
        }
    }

    //checks the position of the character, if he leaves determinate zones
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag.Equals("DangerZone"))
        {
            InsideDangerZone = false;
            dangerCollider = null;
        }

        if (other.gameObject.tag.Equals("DangerBorder"))
        {
            InsideBorder = false;
            borderCollider = null;
        }
        if (other.gameObject.tag.Equals("ForceZone"))
        {
            InsideForceZone = false;
            forceCollider = null;
        }
    }

    public void Reset()
    {
        StopAllCoroutines();

        if (characterCategory != CharacterCategory.Boss)
        {
            gameObject.GetComponent<PlayerAI>().Reset();
            rBody.isKinematic = true;
            agent.ResetPath();
            agent.velocity = Vector3.zero;
            agent.isStopped = false;
        }

        if (healParticle != null)
        {
            healParticle.GetComponent<ParticleSystem>().Stop();
        }
        
        health = MaxHealth;
        gameObject.transform.position = resetPos;
        rBody.velocity = Vector3.zero;
        isForceApplied = false;
        isCasting = false;
        InsideBorder = false;
        InsideDangerZone = false;
        InsideForceZone = false;
        IncomingProjectile = false;
        dangerCollider = null;
        borderCollider = null;
        forceCollider = null;
        borderTimer = 0;

        List<AffectedStat> keys = new List<AffectedStat>(statsModifiers.Keys);
        foreach(AffectedStat stat in keys)
        {
            statsModifiers[stat] = 0;
        }
    }

    public void Pause()
    {
        agent.velocity = Vector3.zero;
        agent.isStopped = true;
    }

    //health management
    public void AddHit(float dmg, string sourceName)
    {
        //on hit, the agent obtains a reward, positive if it isn't the boss, negative otherwise
        if (characterCategory != CharacterCategory.Boss)
        {
            gameObject.GetComponent<PlayerAI>().boss.GetComponent<BossAgent>().AddReward(0.1f);
        }
        else
        {
            gameObject.GetComponent<BossAgent>().AddReward(-0.001f);
            if (InsideBorder)
            {
                gameObject.GetComponent<BossAgent>().AddReward(-0.05f);
            }
        }

        //if defense modified the modifier value will be different from 0 and will change the dmg received
        float modifierValue = -statsModifiers[AffectedStat.Defense];
        float effectiveDmg = dmg + (dmg * modifierValue) / 100;
        health -= effectiveDmg;

        if (characterCategory != CharacterCategory.Boss && logWindow != null && sourceName != null)
        {
            logWindow.GetComponent<GUILog>().AddLog(sourceName + " hits " + characterCategory.ToString() + " for " + effectiveDmg + " damage.");
        }

        if (health <= 0 && characterCategory != CharacterCategory.Boss)
        {
            health = 0;
            gameObject.GetComponent<PlayerAI>().Kill();
        }
    }

    //heals the player for the amount and shows a particle effect
    public void AddHeal(float value, GameObject particle)
    {
        health += value;

        if(health > MaxHealth)
        {
            health = MaxHealth;
        }

        if(healParticle == null)
        {
            healParticle = Instantiate(particle, gameObject.transform.position, Quaternion.identity, gameObject.transform);
        }

        healParticle.GetComponent<ParticleSystem>().Play();

        StartCoroutine(HealParticle(healParticle));
    }

    //boss movement
    public void MoveBoss(Vector3 direction, Vector3 rotation)
    {
        if (!isCasting)
        {
            Vector3 movement = (direction * movementSpeed);
            movement.y = rBody.velocity.y;
            rBody.velocity = movement;
            if(animator != null)
            {
                animator.SetFloat("Speed", rBody.velocity.magnitude);
            }
            gameObject.transform.Rotate(rotation * bossRotationSpeed);
        }
    }

    //player movement
    public void CalculatePathTo(Vector3 pos)
    {
        //movement possible only if moving
        if (!isCasting && !isForceApplied)
        {
            //if the next pos is different from the last one (in particular if surpasses a certain threshold of distance)
            if (Vector3.Distance(nextPos, pos) > 0.5f)
            {
                if(NavMesh.SamplePosition(pos, out NavMeshHit hit, 2, NavMesh.AllAreas))
                {
                    nextPos = hit.position;
                }

                agent.isStopped = false;
                agent.SetDestination(nextPos);

                //if(gameObject.GetComponent<PlayerAI>().ranged)
                //Debug.Log(gameObject.name + " nuova path assegnata, agente fermo: " + agent.isStopped + " rbody kinematico: " + rBody.isKinematic);
            }
        }
    }

    public void ResetPath()
    {
        agent.isStopped = true;
        agent.ResetPath();
    }

    //checks if the player is in the trajectory of a projectile
    private bool SeeIncomingProjectile()
    {
        float radius = gameObject.GetComponent<CapsuleCollider>().radius;
        Vector3 direction = gameObject.GetComponent<PlayerAI>().boss.transform.position - gameObject.transform.position;

        if (Physics.Linecast(gameObject.transform.position, gameObject.GetComponent<PlayerAI>().boss.transform.position, out RaycastHit hit))
        {
            //first checks if the trajectory if free from any wall or other players
            if (hit.collider.tag.Equals("Arena") || hit.collider.tag.Equals("AIPlayer"))
            {
                return false;
            }
            else
            {
                //then if the projectile is actually going to hit the player
                if (Physics.SphereCast(gameObject.transform.position, radius, direction, out RaycastHit sphereHit, 2))
                {
                    if (sphereHit.collider.tag.Equals("BossProjectile"))
                    {
                        bossProjectilePos = sphereHit.collider.transform.position;
                        return true;
                    }
                }
            }
        }

        return false;
    }

    //stop the character until cast is complete or stopped
    public void SetIsCasting(bool cast)
    {
        if (cast)
        {
            //Debug.Log(gameObject.name + " casting");
            isCasting = true;
            if (characterCategory != CharacterCategory.Boss)
            {
                agent.velocity = Vector3.zero;
                agent.isStopped = true;
            }
            rBody.isKinematic = true;
        }
        else
        {
            rBody.isKinematic = false;
            if (characterCategory != CharacterCategory.Boss)
            {
                agent.isStopped = false;
            }
            isCasting = false;
        }
    }

    public void PrepareToApplyForce(float duration)
    {
        isForceApplied = true;
        agent.velocity = Vector3.zero;
        agent.isStopped = true;
        rBody.isKinematic = false;
        StartCoroutine(RestartAgentAfter(duration));
    }

    //coroutine to show the heal particle
    private IEnumerator HealParticle(GameObject particles)
    {
        yield return new WaitForSeconds(2f);

        healParticle.GetComponent<ParticleSystem>().Stop();
    }

    //coroutine to simulate the reaction time of a player to the dangers
    private IEnumerator DangerReaction(float seconds, int dangerType)
    {
        yield return new WaitForSeconds(seconds);

        //border
        if(dangerType == 0)
        {
            InsideBorder = true;
        }
        //aoe attack
        if(dangerType == 1)
        {
            InsideDangerZone = true;
        }
        //incoming projectile
        if(dangerType == 2)
        {
            IncomingProjectile = true;
        }
        //force attack
        if(dangerType == 3)
        {
            InsideForceZone = true;
        }
    }

    private IEnumerator RestartAgentAfter(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        rBody.isKinematic = true;
        agent.isStopped = false;
        isForceApplied = false;
    }
}

//category of the character
public enum CharacterCategory { Boss, Tank, Healer, MeleeDPS, RangedDPS }

#if UNITY_EDITOR
//handles the impector menu of the script
[CustomEditor(typeof(PersonaController))]
public class PesonaControllerEditor : Editor
{
    override public void OnInspectorGUI()
    {
        PersonaController myScript = target as PersonaController;

        myScript.characterCategory = (CharacterCategory)EditorGUILayout.EnumPopup("Character Category", myScript.characterCategory);
        if (myScript.characterCategory == CharacterCategory.Boss)
        {
            myScript.bossRotationSpeed = EditorGUILayout.FloatField("Boss rot speed", myScript.bossRotationSpeed);
        }
        myScript.movementSpeed = EditorGUILayout.FloatField("Movement speed", myScript.movementSpeed);

        myScript.health = EditorGUILayout.FloatField("Character Health", myScript.health);

        if (GUI.changed)
        {
            EditorUtility.SetDirty(myScript);
        }
    }
}

#endif