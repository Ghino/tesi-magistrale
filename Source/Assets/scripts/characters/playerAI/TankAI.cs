﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class TankAI : PlayerAI
{
    protected override void StartCombatTree()
    {
        DTAction a1 = new DTAction(MoveToRightPos);
        DTAction a2 = new DTAction(BuffMyself);
        DTAction a3 = new DTAction(MeleeAttack);
        DTAction a4 = new DTAction(PauseDT);

        DTDecision d1 = new DTDecision(ShouldAttackBoss);
        DTDecision d2 = new DTDecision(AmCloseToMeleeAttack);
        DTDecision d3 = new DTDecision(CanUseBuff);

        d1.AddLink(true, d2);
        d1.AddLink(false, a4);
        d2.AddLink(true, d3);
        d2.AddLink(false, a1);
        d3.AddLink(true, a2);
        d3.AddLink(false, a3);

        combatDT = new DecisionTree(d1);
    }

    protected override void StartCarefulTree()
    {
        DTAction a1 = new DTAction(MoveToRightPos);
        DTAction a2 = new DTAction(PauseDT);

        DTDecision d1 = new DTDecision(AmAtSafeDistance);

        d1.AddLink(false, a1);
        d1.AddLink(true, a2);

        woundedDT = new DecisionTree(d1);
    }



    //DT conditions

    //checks if the player is close enough to use a melee attack
    private object AmCloseToMeleeAttack(object o)
    {
        //getting right distance (melee range, radius of the character and boss) to decide if close enough
        float distance = gameObject.GetComponent<MeleeController>().GetMeleeRange() + gameObject.GetComponent<CapsuleCollider>().radius +
                        (boss.GetComponent<CapsuleCollider>().radius * boss.transform.localScale.x);

        if (Vector3.Distance(gameObject.transform.position, boss.transform.position) <= distance)
        {
            return true;
        }

        nextPos = boss.transform.position;

        return false;
    }

    //checks if the player should go near the boss to attack him
    private object ShouldAttackBoss(object o)
    {
        //if the boss is inside a mark of his next attack
        if (boss.GetComponent<PersonaController>().InsideDangerZone)
        {
            controller.ResetPath();
            return false;
        }
        //if the boss is inside the border and the player has less than 70% of his health he should avoid the boss
        if (boss.GetComponent<PersonaController>().InsideBorder && controller.health < (controller.MaxHealth * 70) / 100)
        {
            controller.ResetPath();
            return false;
        }

        return true;
    }

    //checks if the player can use a buff
    private object CanUseBuff(object o)
    {
        List<GameObject> targets = new List<GameObject>();
        targets.Add(gameObject);

        if (executors[1].CanExecute(targets).Equals(ExecutorErrorCodes.OK))
        {
            return true;
        }

        return false;
    }

    //actions

    private object BuffMyself(object o)
    {
        List<GameObject> targets = new List<GameObject>
        {
            gameObject
        };

        //defense buff
        if (executors[1].CanExecute(targets).Equals(ExecutorErrorCodes.OK))
        {
            executors[1].ExecuteSkill(targets);

            return true;
        }

        return false;
    }

    private object MeleeAttack(object o)
    {
        List<GameObject> targets = new List<GameObject>
        {
            boss
        };

        //melee attack
        executors[0].ExecuteSkill(targets);

        return true;
    }
}
