﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSM
{
    public FSMState current;

    public FSM(FSMState state)
    {
        current = state;
        current.Enter();
    }

    public void Update()
    {
        //examine transition to eventually exit the state
        FSMTransition transition = current.VerifyTransitions();

        //if there is one activated
        if(transition != null)
        {
            //do all exit actions of the current state
            current.Exit();

            //do the transition actions
            transition.Fire();

            //getting the next state
            current = current.NextState(transition);

            //do all entry actions of the new state
            current.Enter();
        }
        else
        {
            //if no transiction are activated do all stay actions of the current state
            current.Stay();
        }
    }
}


//delegates to incapsulate conditions and action methods
public delegate bool FSMCondition();

public delegate void FSMAction();



public class FSMTransition
{
    public FSMCondition myCondition;

    private List<FSMAction> myActions = new List<FSMAction>();

    public FSMTransition(FSMCondition condition, List<FSMAction> actions = null)
    {
        myCondition = condition;

        if (actions != null)
        {
            myActions.AddRange(actions);
        }
    }

    public void Fire()
    {
        if (myActions != null)
        {
            foreach (FSMAction action in myActions)
            {
                action();
            }
        }
    }
}



public class FSMState
{
    public List<FSMAction> entryActions = new List<FSMAction>();
    public List<FSMAction> stayActions = new List<FSMAction>();
    public List<FSMAction> exitActions = new List<FSMAction>();

    //list of the state transitions to other states
    private Dictionary<FSMTransition, FSMState> links;

    public FSMState()
    {
        links = new Dictionary<FSMTransition, FSMState>();
    }

    public void AddTransition(FSMTransition transition, FSMState targetState)
    { 
        links[transition] = targetState;
    }

    public FSMTransition VerifyTransitions()
    {
        foreach (FSMTransition transition in links.Keys)
        {
            if (transition.myCondition())
            {
                return transition;
            }
        }

        return null;
    }

    public FSMState NextState(FSMTransition transition)
    {
        return links[transition];
    }

    //exectuions of all the actions in the entry, stay and exit lists
    public void Enter()
    {
        foreach (FSMAction action in entryActions)
        {
            action();
        }
    }

    public void Stay()
    {
        foreach (FSMAction action in stayActions)
        {
            action();
        }
    }

    public void Exit()
    {
        foreach (FSMAction action in exitActions)
        {
            action();
        }
    }
}
