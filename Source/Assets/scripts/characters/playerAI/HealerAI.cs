﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class HealerAI : PlayerAI
{
    public float aggressiveDistFromBoss = 10f;
    private List<GameObject> woundedMembers = new List<GameObject>();
    private GameObject mostWounded;

    //method to find the most wounded of the party members (by health percentage)
    private GameObject GetMostWounded()
    {
        GameObject mostWounded = woundedMembers[0];
        foreach (GameObject member in woundedMembers)
        {
            float healthPercentageMW = (mostWounded.GetComponent<PersonaController>().health * 100) / mostWounded.GetComponent<PersonaController>().MaxHealth;
            float healthwoundsPercentageM = (member.GetComponent<PersonaController>().health * 100) / member.GetComponent<PersonaController>().MaxHealth;
            if (healthwoundsPercentageM < healthPercentageMW)
            {
                mostWounded = member;
            }
        }

        return mostWounded;
    }

    protected override void StartCombatTree()
    {
        DTAction a1 = new DTAction(MoveToRightPos);
        DTAction a2 = new DTAction(RangedAttack);
        DTAction a3 = new DTAction(Heal);
        
        DTDecision d1 = new DTDecision(AmAtCombatDistance);
        DTDecision d2 = new DTDecision(BossOnSight);
        DTDecision d3 = new DTDecision(NeedHeal);
        DTDecision d4 = new DTDecision(PlayerOnSight);
        DTDecision d5 = new DTDecision(AmCloseToHeal);
        DTDecision d6 = new DTDecision(CanHeal);
        DTDecision d7 = new DTDecision(IsBossInBorder);
        DTDecision d8 = new DTDecision(AmNearBorder);

        //healing branch (has priority)
        //check if heal is needed
        d3.AddLink(false, d2);
        d3.AddLink(true, d6);

        //check if i can heal
        d6.AddLink(true, d5);
        d6.AddLink(false, d2);

        //check if i'm close to heal
        d5.AddLink(true, d4);
        d5.AddLink(false, a1);

        //check if i can see the player to heal him
        d4.AddLink(true, a3);
        d4.AddLink(false, a1);

        //offensive branch
        //check if i can see the boss
        d2.AddLink(true, d7);
        d2.AddLink(false, a1);

        //check if boss is in border
        d7.AddLink(true, d8);
        d7.AddLink(false, d1);

        //check if i respect the distance from the border by attacking the boss near the border
        d8.AddLink(true, a2);
        d8.AddLink(false, a1);

        //check if i respect the distance from the boss
        d1.AddLink(true, a2);
        d1.AddLink(false, a1);

        //starting from the check if healing is needed
        combatDT = new DecisionTree(d3);
    }

    protected override void StartCarefulTree()
    {
        DTAction a1 = new DTAction(MoveToRightPos);
        DTAction a2 = new DTAction(HealMyself);
        DTAction a3 = new DTAction(RangedAttack);
        
        DTDecision d1 = new DTDecision(AmAtSafeDistance);
        DTDecision d2 = new DTDecision(CanHeal);
        DTDecision d3 = new DTDecision(BossOnSight);

        d1.AddLink(false, a1);
        d1.AddLink(true, a3);
        d2.AddLink(true, a2);
        d2.AddLink(false, d3);
        d3.AddLink(true, d1);
        d3.AddLink(false, a1);

        woundedDT = new DecisionTree(d2);
    }

    //DT conditions
    
    //checks if there is a wounded party member
    private object NeedHeal(object o)
    {
        woundedMembers.Clear();

        foreach (GameObject member in partyMemebers)
        {
            if (member.activeSelf)
            {
                float health = member.GetComponent<PersonaController>().health;
                float maxHealth = member.GetComponent<PersonaController>().MaxHealth;
                //health under 75%
                float healthThreshold = (maxHealth * 75) / 100;

                if (health < healthThreshold)
                {
                    woundedMembers.Add(member);
                }
            }
        }

        if (woundedMembers.Count >= 1)
        {
            return true;
        }
        return false;
    }

    //checks if at least one healing skills is avaliable
    private object CanHeal(object o)
    {
        if (executors[1].CanExecute(null).Equals(ExecutorErrorCodes.OK) || executors[2].CanExecute(null).Equals(ExecutorErrorCodes.OK))
        {
            return true;
        }
        return false;
    }

    //checks if the player is enoght close to the wounded player/s
    private object AmCloseToHeal(object o)
    {
        //if there is only one wounded go near him
        if (woundedMembers.Count == 1)
        {
            if (Vector3.Distance(gameObject.transform.position, woundedMembers[0].transform.position) <= moves[1].maxRange + controller.stoppingDistance)
            {
                return true;
            }
            nextPos = (woundedMembers[0].transform.position - gameObject.transform.position).normalized * moves[1].maxRange;
        }

        //if there are more wounded players go near the middle point
        //only 2
        if (woundedMembers.Count == 2)
        {
            float maxDistance = moves[2].maxRange + controller.stoppingDistance;
            if (Vector3.Distance(gameObject.transform.position, woundedMembers[0].transform.position) <= maxDistance && Vector3.Distance(gameObject.transform.position, woundedMembers[1].transform.position) <= maxDistance)
            {
                return true;
            }

            Vector3 middlePos = (woundedMembers[0].transform.position + woundedMembers[1].transform.position) / 2;

            //check if all wounded member are near the middle pos enough (considering the healing range)
            if (Vector3.Distance(middlePos, woundedMembers[0].transform.position) <= maxDistance && Vector3.Distance(middlePos, woundedMembers[1].transform.position) <= maxDistance)
            {
                //if i am really close to the middle pos
                if (Vector3.Distance(gameObject.transform.position, middlePos) <= controller.stoppingDistance + 1)
                {
                    return true;
                }
                nextPos = middlePos;
            }
            else //else go near the most wounded to heal him
            {
                GameObject mostWounded = GetMostWounded();

                if (Vector3.Distance(gameObject.transform.position, mostWounded.transform.position) < maxDistance)
                {
                    return true;
                }
                nextPos = mostWounded.transform.position;
            }
        }

        //3 or more
        if (woundedMembers.Count >= 3)
        {
            float maxDistance = moves[2].maxRange + controller.stoppingDistance;
            float minX = woundedMembers[0].transform.position.x;
            float minY = woundedMembers[0].transform.position.y;
            float minZ = woundedMembers[0].transform.position.z;

            float maxX = woundedMembers[0].transform.position.x;
            float maxY = woundedMembers[0].transform.position.y;
            float maxZ = woundedMembers[0].transform.position.z;

            bool alreadyCloseToEveryone = true;

            //finding the middle point of each wounded player
            foreach (GameObject member in woundedMembers)
            {
                if (Vector3.Distance(gameObject.transform.position, member.transform.position) > maxDistance)
                {
                    alreadyCloseToEveryone = false;
                }

                if (member.transform.position.x < minX)
                {
                    minX = member.transform.position.x;
                }
                if (member.transform.position.y < minY)
                {
                    minY = member.transform.position.y;
                }
                if (member.transform.position.z < minZ)
                {
                    minZ = member.transform.position.z;
                }

                if (member.transform.position.x > maxX)
                {
                    maxX = member.transform.position.x;
                }
                if (member.transform.position.y > maxY)
                {
                    maxY = member.transform.position.y;
                }
                if (member.transform.position.z > maxZ)
                {
                    maxZ = member.transform.position.z;
                }
            }

            if (alreadyCloseToEveryone)
            {
                return true;
            }

            Vector3 middlePos = new Vector3((minX + maxX) / 2, (minY + maxY) / 2, (minZ + maxZ) / 2);
            
            bool allNearMiddlePos = true;

            //check if all wounded member are near the middle pos enough (considering the healing range)
            foreach (GameObject member in woundedMembers)
            {
                if (Vector3.Distance(middlePos, member.transform.position) > maxDistance)
                {
                    allNearMiddlePos = false;
                    break;
                }
            }

            if (allNearMiddlePos)
            {
                //if i am really close to the middle pos
                if (Vector3.Distance(gameObject.transform.position, middlePos) <= controller.stoppingDistance + 1)
                {
                    return true;
                }
                
                nextPos = middlePos;
            }
            else //else go near the most wounded to heal him
            {
                GameObject mostWounded = GetMostWounded();

                if (Vector3.Distance(gameObject.transform.position, mostWounded.transform.position) <= maxDistance)
                {
                    return true;
                }
                
                nextPos = mostWounded.transform.position;
            }
        }

        return false;
    }

    //the same as the ranged DPS
    private object AmAtCombatDistance(object o)
    {
        float playerDistance = Vector3.Distance(gameObject.transform.position, boss.transform.position);
        if (playerDistance >= aggressiveDistFromBoss - controller.stoppingDistance && playerDistance <= aggressiveDistFromBoss + controller.stoppingDistance)
        {
            return true;
        }

        Vector3 direction = transform.position - boss.transform.position;
        nextPos = boss.transform.position + direction.normalized * aggressiveDistFromBoss;
        nextPos = new Vector3(nextPos.x, gameObject.transform.position.y, nextPos.z);

        return false;
    }

    //check if i can see the player (always true for an aoe healing)
    private object PlayerOnSight(object o)
    {
        //if i need to use the multiple heal i don't need eye contact since it's AOE
        if(executors[2].CanExecute(null).Equals(ExecutorErrorCodes.OK) && woundedMembers.Count > 1)
        {
            return true;
        }

        GameObject mostWounded = GetMostWounded();
        
        LayerMask arenaLayer = (1 << 11);
        if (Physics.Linecast(gameObject.transform.position, mostWounded.transform.position, arenaLayer))
        {
            nextPos = mostWounded.transform.position;
            return false;
        }

        return true;
    }

    //same as ranged DPS
    private object BossOnSight(object o)
    {
        LayerMask arenaLayer = (1 << 11);
        if (Physics.Linecast(gameObject.transform.position, boss.transform.position, arenaLayer))
        {
            nextPos = boss.transform.position;
            return false;
        }
        return true;
    }

    //same as ranged DPS
    private object IsBossInBorder(object o)
    {
        if (boss.GetComponent<PersonaController>().InsideBorder)
        {
            return true;
        }
        return false;
    }

    //same as ranged DPS
    private object AmNearBorder(object o)
    {
        GameObject borderSafeZones = safeZones[0];
        for (int i = 0; i < borderSafeZones.transform.childCount; i++)
        {
            if (Vector3.Distance(boss.transform.position, borderSafeZones.transform.GetChild(i).position) >= safeDistFromBoss)
            {
                nextPos = borderSafeZones.transform.GetChild(i).position;
                break;
            }
        }

        for (int i = 0; i < borderSafeZones.transform.childCount; i++)
        {
            if (Vector3.Distance(boss.transform.position, borderSafeZones.transform.GetChild(i).position) >= aggressiveDistFromBoss)
            {
                float tmpDistance = Vector3.Distance(boss.transform.position, borderSafeZones.transform.GetChild(i).position);
                if (tmpDistance < Vector3.Distance(boss.transform.position, nextPos))
                {
                    nextPos = borderSafeZones.transform.GetChild(i).position;
                }
            }
        }

        if (Vector3.Distance(gameObject.transform.position, nextPos) <= controller.stoppingDistance + 1)
        {
            return true;
        }

        return false;
    }

    //actions
    
    //heal the player/s by first checking if the aoe is needed and can be executed, if not use the single heal to the most wounded
    private object Heal(object o)
    {
        List<GameObject> targets = new List<GameObject>(partyMemebers);
        if (executors[2].CanExecute(null).Equals(ExecutorErrorCodes.OK) && woundedMembers.Count > 1)
        {
            targets.Add(gameObject);
            executors[2].ExecuteSkill(targets);

            return true;
        }

        
        if (executors[1].CanExecute(null).Equals(ExecutorErrorCodes.OK))
        {
            //if there are more members wounded and i can only use the single heal find the most wounded one
            GameObject mostWounded = GetMostWounded();
            targets.Clear();
            targets.Add(mostWounded);
            executors[1].ExecuteSkill(targets);
        }
        else
        {
            targets.Add(gameObject);
            executors[2].ExecuteSkill(partyMemebers);
        }
        
        return true;
    }

    //when i am wounded i heal myself first
    private object HealMyself(object o)
    {
        List<GameObject> targets = new List<GameObject>
        {
            gameObject
        };

        //if single not avaliable use multiple
        if (!executors[1].CanExecute(targets).Equals(ExecutorErrorCodes.CD))
        {
            executors[1].ExecuteSkill(targets);
        }

        if (!executors[2].CanExecute(targets).Equals(ExecutorErrorCodes.CD))
        {
            executors[2].ExecuteSkill(targets);
        }

        return true;
    }

    //ranged attack

    private object RangedAttack(object o)
    {
        List<GameObject> targets = new List<GameObject>
        {
            boss
        };
        executors[0].ExecuteSkill(targets);

        return true;
    }
}
