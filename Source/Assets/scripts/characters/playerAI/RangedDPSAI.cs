﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class RangedDPSAI : PlayerAI
{
    public float aggressiveDistFromBoss = 10f;

    protected override void StartCombatTree()
    {
        DTAction a1 = new DTAction(MoveToRightPos);
        DTAction a2 = new DTAction(BuffMyself);
        DTAction a3 = new DTAction(RangedAttack);
        
        DTDecision d1 = new DTDecision(AmAtCombatDistance);
        DTDecision d2 = new DTDecision(CanUseBuff);
        DTDecision d3 = new DTDecision(BossOnSight);
        DTDecision d4 = new DTDecision(IsBossInBorder);
        DTDecision d5 = new DTDecision(AmNearBorder);

        //check if boss is on sight
        d3.AddLink(true, d4);
        d3.AddLink(false, a1);

        //check if boss is in border
        d4.AddLink(true, d5);
        d4.AddLink(false, d1);

        //check if i respect the distance from boss considering the border
        d5.AddLink(true, d2);
        d5.AddLink(false, a1);

        //check if i respect distance from boss
        d1.AddLink(true, d2);
        d1.AddLink(false, a1);

        //check if i can use buff
        d2.AddLink(true, a2);
        d2.AddLink(false, a3);

        combatDT = new DecisionTree(d3);
    }

    protected override void StartCarefulTree()
    {
        DTAction a1 = new DTAction(MoveToRightPos);
        DTAction a2 = new DTAction(RangedAttack);

        DTDecision d1 = new DTDecision(AmAtSafeDistance);
        DTDecision d2 = new DTDecision(BossOnSight);

        d1.AddLink(false, a1);
        d1.AddLink(true, a2);
        d2.AddLink(true, d1);
        d2.AddLink(false, a1);

        woundedDT = new DecisionTree(d2);
    }

    //DT conditions

    //checks if the player mantains the correct distance to attack the boss
    private object AmAtCombatDistance(object o)
    {
        float playerDistance = Vector3.Distance(gameObject.transform.position, boss.transform.position);
        if (playerDistance >= aggressiveDistFromBoss - controller.stoppingDistance && playerDistance <= aggressiveDistFromBoss + controller.stoppingDistance)
        {
            return true;
        }

        Vector3 direction = transform.position - boss.transform.position;
        nextPos = boss.transform.position + direction.normalized * aggressiveDistFromBoss;
        nextPos = new Vector3(nextPos.x, gameObject.transform.position.y, nextPos.z);

        return false;
    }

    private object CanUseBuff(object o)
    {
        List<GameObject> targets = new List<GameObject>();
        targets.Add(gameObject);

        if (executors[1].CanExecute(targets).Equals(ExecutorErrorCodes.OK))
        {
            return true;
        }

        return false;
    }

    //check if the player can see the boss
    private object BossOnSight(object o)
    {
        LayerMask arenaLayer = (1 << 11);
        if(Physics.Linecast(gameObject.transform.position, boss.transform.position, arenaLayer))
        {
            nextPos = boss.transform.position;
            return false;
        }
        
        return true;
    }

    //check if the boss is inside the border of the arena
    private object IsBossInBorder(object o)
    {
        if (boss.GetComponent<PersonaController>().InsideBorder)
        {
            return true;
        }

        return false;
    }

    //checks if the player is in the right position to attack the boss who is in the border, by staying near the border without going inside
    private object AmNearBorder(object o)
    {
        GameObject borderSafeZones = safeZones[0];
        for (int i = 0; i < borderSafeZones.transform.childCount; i++)
        {
            if (Vector3.Distance(boss.transform.position, borderSafeZones.transform.GetChild(i).position) >= safeDistFromBoss)
            {
                nextPos = borderSafeZones.transform.GetChild(i).position;
                break;
            }
        }

        for (int i = 0; i < borderSafeZones.transform.childCount; i++)
        {
            if (Vector3.Distance(boss.transform.position, borderSafeZones.transform.GetChild(i).position) >= aggressiveDistFromBoss)
            {
                float tmpDistance = Vector3.Distance(boss.transform.position, borderSafeZones.transform.GetChild(i).position);
                if (tmpDistance < Vector3.Distance(boss.transform.position, nextPos))
                {
                    nextPos = borderSafeZones.transform.GetChild(i).position;
                }
            }
        }

        if (Vector3.Distance(gameObject.transform.position, nextPos) <= controller.stoppingDistance + 1)
        {
            return true;
        }

        return false;
    }


    //BT actions

    private object RangedAttack(object o)
    {
        List<GameObject> targets = new List<GameObject>
        {
            boss
        };

        //ranged attack
        executors[0].ExecuteSkill(targets);

        return true;
    }

    private object BuffMyself(object o)
    {
        List<GameObject> targets = new List<GameObject>
        {
            gameObject
        };

        //attack buff
        executors[1].ExecuteSkill(targets);

        return true;
    }
}
