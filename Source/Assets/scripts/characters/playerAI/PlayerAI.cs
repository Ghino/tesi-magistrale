﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

//class responsible for all the players AI (the common actions)
public abstract class PlayerAI : MonoBehaviour
{
    protected Coroutine inst = null;
    private FSM highFSM;
    private FSM safeFSM;
    protected DecisionTree combatDT;
    protected DecisionTree woundedDT;
    protected DecisionTree dangerDT;

    protected PersonaController controller;
    public float AIReactionTime = 0.01f;
    public bool ranged = false;
    public bool isHealer = false;

    public List<Move> moves;
    protected List<SkillExecutor> executors = new List<SkillExecutor>();

    public GameObject boss;
    public List<GameObject> partyMemebers;
    public float safeDistFromBoss = 10f;
    public List<GameObject> safeZones = new List<GameObject>();

    protected Vector3 nextPos;
    private bool isDodging = false;

    void Start()
    {
        controller = gameObject.GetComponent<PersonaController>();

        AIReactionTime /= Time.timeScale;

        //skills setup
        foreach (Move m in moves)
        {
            SkillExecutor executor = (SkillExecutor)gameObject.AddComponent(Type.GetType(m.executorName));
            executor.Initialize(m);
            executors.Add(executor);
        }

        //FSM

        //safeFSM
        FSMState stopped = new FSMState();
        stopped.stayActions.Add(Pause);

        FSMState inCombat = new FSMState();
        inCombat.entryActions.Add(StartCombatTree);
        inCombat.stayActions.Add(WalkCombatTree);

        FSMState careful = new FSMState();
        careful.entryActions.Add(StartCarefulTree);
        careful.stayActions.Add(WalkCarefulTree);

        FSMTransition tFighting = new FSMTransition(AmFighting);
        FSMTransition tBossDead = new FSMTransition(IsBossDead);
        FSMTransition tLowHealth = new FSMTransition(ShouldBeCareful);
        FSMTransition tHighHealth = new FSMTransition(ShouldBeAggressive);
        FSMTransition tDead = new FSMTransition(AmDead);
        FSMTransition tAlive = new FSMTransition(AmAlive);

        stopped.AddTransition(tFighting, inCombat);
        inCombat.AddTransition(tBossDead, stopped);
        inCombat.AddTransition(tLowHealth, careful);
        inCombat.AddTransition(tDead, stopped);
        careful.AddTransition(tHighHealth, inCombat);
        careful.AddTransition(tBossDead, stopped);
        careful.AddTransition(tDead, stopped);

        safeFSM = new FSM(stopped);

        //highFSM
        FSMState inDanger = new FSMState();
        inDanger.entryActions.Add(StopCasting);
        inDanger.entryActions.Add(StartDangerTree);
        inDanger.stayActions.Add(WalkDangerTree);

        FSMState safe = new FSMState();
        safe.stayActions.Add(safeFSM.Update);

        FSMState disabled = new FSMState();
        disabled.stayActions.Add(Pause);

        FSMTransition tSafe = new FSMTransition(AmSafe);
        FSMTransition tDanger = new FSMTransition(AmInDanger);
        FSMTransition tDisabled = new FSMTransition(AmDisabled);
        FSMTransition tEnabledSafe = new FSMTransition(AmEnabledAndSafe);
        FSMTransition tEnabledInDanger = new FSMTransition(AmEnabledAndInDanger);

        inDanger.AddTransition(tSafe, safe);
        inDanger.AddTransition(tDisabled, disabled);

        safe.AddTransition(tDanger, inDanger);
        safe.AddTransition(tDisabled, disabled);

        disabled.AddTransition(tEnabledSafe, safe);
        disabled.AddTransition(tEnabledInDanger, inDanger);

        highFSM = new FSM(safe);

        inst = StartCoroutine(Fight());
    }

    //called when the player dies
    public void Kill()
    {
        //boss.GetComponent<BossAgent>().AddReward(0.5f);
        StopAllCoroutines();
        gameObject.SetActive(false);
    }

    //reset the skills and the coroutine
    public void Reset()
    {
        foreach(SkillExecutor executor in executors)
        {
            executor.Reset();
        }
        inst = StartCoroutine(Fight());
    }

    private IEnumerator Fight()
    {
        while (true)
        {
            highFSM.Update();
            yield return new WaitForSeconds(AIReactionTime);
        }
    }


    //FSM conditions
    //check if there is an immediate danger
    private bool AmInDanger()
    {
        //if player is inside an incoming aoe attack
        if (controller.InsideDangerZone)
        {
            return true;
        }

        //if player is on the dangerous border
        //ranged players should always avoid the border (since they can attack from distance) 
        //while melee should avoid the border only under a certain threshold of health
        if (ranged)
        {
            if (controller.InsideBorder)
            {
                return true;
            }
        }
        else
        {
            if (controller.InsideBorder && controller.health < (controller.MaxHealth * 70) / 100)
            {
                return true;
            }
        }

        //if the player sees an incoming projectile
        if (controller.IncomingProjectile)
        {
            return true;
        }

        //if he is inside the range of a force attack
        if (controller.InsideForceZone)
        {
            return true;
        }

        isDodging = false;

        return false;
    }

    private bool AmSafe()
    {
        return !AmInDanger();
    }

    private bool AmFighting()
    {
        return IsBossAlive() && AmAlive();
    }

    private bool IsBossAlive()
    {
        if (boss.GetComponent<PersonaController>().health > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private bool IsBossDead()
    {
        return !IsBossAlive();
    }

    //health below 30% and healer is alive
    private bool ShouldBeCareful()
    {
        if (controller.health < (controller.MaxHealth * 30) / 100)
        {
            foreach(GameObject member in partyMemebers)
            {
                if (member.GetComponent<PlayerAI>().isHealer && member.activeSelf)
                {
                    return true;
                }
            }
        }

        return false;
    }

    private bool ShouldBeAggressive()
    {
        return !ShouldBeCareful();
    }

    private bool AmDead()
    {
        if (controller.health <= 0)
        {
            return true;
        }

        return false;
    }

    private bool AmAlive()
    {
        return !AmDead();
    }

    private bool AmDisabled()
    {
        if(controller.isCasting || controller.isForceApplied)
        {
            return true;
        }

        return false;
    }

    private bool AmEnabledAndSafe()
    {
        return !AmDisabled() && AmSafe();
    }

    private bool AmEnabledAndInDanger()
    {
        return !AmDisabled() && AmInDanger();
    }

    //FSM actions
    private void Pause()
    {
        //do nothing

    }

    //stop cast to move to avoid danger
    private void StopCasting()
    {
        if (controller.isCasting)
        {
            foreach (SkillExecutor executor in executors)
            {
                executor.StopCast();
            }
            controller.SetIsCasting(false);
        }
    }

    //decision tree used to decide which danger and how to avoid it
    protected virtual void StartDangerTree()
    {
        DTAction a1 = new DTAction(MoveToRightPos);
        DTAction a2 = new DTAction(DodgeToPos);

        DTDecision d1 = new DTDecision(InsideDangerZone);
        DTDecision d2 = new DTDecision(InsideBorder);
        DTDecision d3 = new DTDecision(ProjectileIncoming);
        DTDecision d4 = new DTDecision(InsideForceZone);

        d1.AddLink(true, a1);
        d1.AddLink(false, d4);
        d4.AddLink(true, a1);
        d4.AddLink(false, d3);
        d3.AddLink(true, a2);
        d3.AddLink(false, d2);
        d2.AddLink(true, a1);

        dangerDT = new DecisionTree(d1);
    }

    private void WalkDangerTree()
    {
        dangerDT.walk();
    }

    //decision tree used to decide how to fight aggressively
    protected abstract void StartCombatTree();

    private void WalkCombatTree()
    {
        combatDT.walk();
    }

    //decision tree used to decide how to fight when wounded
    protected abstract void StartCarefulTree();

    private void WalkCarefulTree()
    {
        woundedDT.walk();
    }



    //common conditions for the DT
    //checks if the player is inside the border of the arena and finds the next position to reach
    protected object InsideBorder(object o)
    {
        if (controller.InsideBorder)
        {
            //choose the nearest safe zone near the border and enough distant from the boss to keep the safe distance since the character is wounded or is ranged
            GameObject borderSafeZones = safeZones[0];
            for (int i = 0; i < borderSafeZones.transform.childCount; i++)
            {
                if (Vector3.Distance(boss.transform.position, borderSafeZones.transform.GetChild(i).position) >= safeDistFromBoss)
                {
                    nextPos = borderSafeZones.transform.GetChild(i).position;
                    break;
                }
            }
            
            for (int i = 0; i < borderSafeZones.transform.childCount; i++)
            {
                if (Vector3.Distance(boss.transform.position, borderSafeZones.transform.GetChild(i).position) >= safeDistFromBoss)
                {
                    float tmpDistance = Vector3.Distance(gameObject.transform.position, borderSafeZones.transform.GetChild(i).position);
                    if (tmpDistance < Vector3.Distance(gameObject.transform.position, nextPos))
                    {
                        nextPos = borderSafeZones.transform.GetChild(i).position;
                    }
                }
            }

            return true;
        }

        return false;
    }

    //checks if the player is inside a mark of an attack and finds the next position to reach
    protected object InsideDangerZone(object o)
    {
        if (controller.InsideDangerZone)
        {
            //go away from the mark which indicates the zone of the attack
            nextPos = transform.position - controller.dangerZonePos;

            return true;
        }

        return false;
    }

    //checks if a projectile is coming to the player and decides the next position to reach to avoid it
    protected object ProjectileIncoming(object o)
    {
        if (controller.IncomingProjectile)
        {
            if (!isDodging)
            {
                isDodging = true;
                Vector3 direction = gameObject.GetComponent<PersonaController>().bossProjectilePos - gameObject.transform.position;
                Vector3 side = Vector3.Cross(new Vector3(0, 1, 0), direction.normalized);
                //nextPos will be a random pos between left and right of the direction of the incoming projectile
                if (Random.Range(0, 2) == 0)
                {
                    direction = side;
                }
                else
                {
                    direction = -side;
                }

                nextPos = direction * 3;
            }
            return true;
        }

        isDodging = false;

        return false;
    }

    //checks if the player is in range of a force attack and find the closest point to avoid it
    protected object InsideForceZone(object o)
    {
        if (controller.InsideForceZone)
        {
            //find the closest safe zone behind a pillar
            GameObject pillarSafeZones = safeZones[1];
            for (int i = 0; i < pillarSafeZones.transform.childCount; i++)
            {
                if (pillarSafeZones.transform.GetChild(i).gameObject.GetComponent<SafeZoneChecker>().isSafe)
                {
                    nextPos = pillarSafeZones.transform.GetChild(i).position;
                    break;
                }
            }
            for (int i = 0; i < pillarSafeZones.transform.childCount; i++)
            {
                GameObject tmpSafeZone = pillarSafeZones.transform.GetChild(i).gameObject;
                if (tmpSafeZone.GetComponent<SafeZoneChecker>().isSafe)
                {
                    float tmpDistance = Vector3.Distance(gameObject.transform.position, pillarSafeZones.transform.GetChild(i).position);
                    if (tmpDistance < Vector3.Distance(gameObject.transform.position, nextPos))
                    {
                        nextPos = pillarSafeZones.transform.GetChild(i).position;
                    }
                }
            }

            return true;
        }

        return false;
    }

    //checks if the player mantains a safe distance from the boss when wounded
    protected object AmAtSafeDistance(object o)
    {
        float playerDistance = Vector3.Distance(gameObject.transform.position, boss.transform.position);
        if (playerDistance >= safeDistFromBoss - controller.stoppingDistance)
        {
            return true;
        }

        Vector3 direction = transform.position - boss.transform.position;
        nextPos = boss.transform.position + direction.normalized * safeDistFromBoss;
        nextPos = new Vector3(nextPos.x, gameObject.transform.position.y, nextPos.z);

        return false;
    }

    //actions
    //pause for DT syntax
    protected object PauseDT(object o)
    {
        //stop moving
        controller.Pause();
        return true;
    }

    //used to notify the player controller the next position to be reached
    protected object MoveToRightPos(object o)
    {
        gameObject.GetComponent<PersonaController>().CalculatePathTo(nextPos);

        return true;
    }

    protected object DodgeToPos(object o)
    {
        gameObject.GetComponent<PersonaController>().PrepareToApplyForce(0.1f);
        gameObject.GetComponent<Rigidbody>().AddForce(nextPos, ForceMode.VelocityChange);

        return true;
    }
}
