﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class MeleeDPSAI : PlayerAI
{
    protected override void StartDangerTree()
    {
        DTAction a1 = new DTAction(MoveToRightPos);
        DTAction a2 = new DTAction(JumpAway);
        DTAction a3 = new DTAction(DodgeToPos);

        DTDecision d1 = new DTDecision(InsideDangerZone);
        DTDecision d2 = new DTDecision(InsideBorder);
        DTDecision d3 = new DTDecision(ProjectileIncoming);
        DTDecision d4 = new DTDecision(InsideForceZone);
        DTDecision d5 = new DTDecision(CanJumpAway);

        d1.AddLink(true, d5);
        d1.AddLink(false, d4);
        d5.AddLink(true, a2);
        d5.AddLink(false, a1);
        d4.AddLink(true, a1);
        d4.AddLink(false, d3);
        d3.AddLink(true, a3);
        d3.AddLink(false, d2);
        d2.AddLink(true, d5);

        dangerDT = new DecisionTree(d1);
    }

    protected override void StartCombatTree()
    {
        DTAction a1 = new DTAction(MoveToRightPos);
        DTAction a2 = new DTAction(GapCloser);
        DTAction a3 = new DTAction(MeleeAttack);
        DTAction a4 = new DTAction(PauseDT);
        
        DTDecision d1 = new DTDecision(ShouldAttackBoss);
        DTDecision d2 = new DTDecision(AmCloseToMeleeAttack);
        DTDecision d3 = new DTDecision(CanGapCloser);

        d1.AddLink(true, d2);
        d1.AddLink(false, a4);
        d2.AddLink(true, a3);
        d2.AddLink(false, d3);
        d3.AddLink(true, a2);
        d3.AddLink(false, a1);

        combatDT = new DecisionTree(d1);
    }

    protected override void StartCarefulTree()
    {
        DTAction a1 = new DTAction(MoveToRightPos);
        DTAction a2 = new DTAction(JumpAway);
        DTAction a3 = new DTAction(PauseDT);
        
        DTDecision d1 = new DTDecision(AmAtSafeDistance);
        DTDecision d2 = new DTDecision(CanJumpAway);

        d1.AddLink(false, d2);
        d1.AddLink(true, a3);
        d2.AddLink(true, a2);
        d2.AddLink(false, a1);

        woundedDT = new DecisionTree(d1);
    }

    //DT conditions

    //same of the tank AI
    private object AmCloseToMeleeAttack(object o)
    {
        float distance = gameObject.GetComponent<MeleeController>().GetMeleeRange() + gameObject.GetComponent<CapsuleCollider>().radius +
                        (boss.GetComponent<CapsuleCollider>().radius * boss.transform.localScale.x);

        if (Vector3.Distance(gameObject.transform.position, boss.transform.position) <= distance)
        {
            return true;
        }
        nextPos = boss.transform.position;

        return false;
    }

    //same of the tank AI
    private object ShouldAttackBoss(object o)
    {
        if (boss.GetComponent<PersonaController>().InsideDangerZone)
        {
            controller.ResetPath();
            return false;
        }

        if (boss.GetComponent<PersonaController>().InsideBorder && controller.health < (controller.MaxHealth * 70) / 100)
        {
            controller.ResetPath();
            return false;
        }

        return true;
    }

    //checks if the player can use the skill to jump away
    private object CanJumpAway(object o)
    {
        List<GameObject> targets = new List<GameObject>()
        {
            boss
        };

        if (executors[2].CanExecute(targets).Equals(ExecutorErrorCodes.OK))
        {
            return true;
        }

        return false;
    }

    //checks if the player can use the skill to close the gap between him and the boss
    private object CanGapCloser(object o)
    {
        List<GameObject> targets = new List<GameObject>()
        {
            boss
        };

        if (executors[1].CanExecute(targets).Equals(ExecutorErrorCodes.OK))
        {
            return true;
        }

        return false;
    }

    //BT actions

    private object MeleeAttack(object o)
    {
        List<GameObject> targets = new List<GameObject>
        {
            boss
        };
        executors[0].ExecuteSkill(targets);

        return true;
    }

    private object GapCloser(object o)
    {
        List<GameObject> targets = new List<GameObject>
        {
            boss
        };
        executors[1].ExecuteSkill(targets);
        return true;
    }

    private object JumpAway(object o)
    {
        List<GameObject> targets = new List<GameObject>
        {
            boss
        };
        executors[2].ExecuteSkill(targets);
        return true;
    }
}
