﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;
using System.IO;

public class BossAgent : Agent
{
    public GameObject arena;

    //enemies of the boss
    public List<GameObject> players;

    private Rigidbody rBody;
    //max distance of the arena
    private float maxDistance;

    public List<Move> bossMoves = new List<Move>();
    private List<SkillExecutor> executors = new List<SkillExecutor>();

    //timer to avoid multiple input of attacks
    public float cdBetweenAttacks = 0.3f;
    private float checkCdBA = 0;

    private Vector3 resetPos;

    //rays of the boss sight
    private RayPerception3D rayPerception;

    //center of the arean
    public GameObject arenaCenter { get; private set; }

    //true if the boss activated the skill to transit to the second phase
    public bool pillarActivated { get; private set; } = false;

    //log info
    private bool notFirstReset = false;
    private float fightDuration = 0;
    private float tmpDuration = 0;

    // Start is called before the first frame update
    void Start()
    {
        rayPerception = GetComponent<RayPerception3D>();
        resetPos = gameObject.transform.position;
        rBody = GetComponent<Rigidbody>();

        Vector3 min = arena.GetComponent<MeshCollider>().bounds.min;
        Vector3 max = arena.GetComponent<MeshCollider>().bounds.max;

        maxDistance = Vector3.Distance(min, max);

        for (int i = 0; i < arena.transform.childCount; i++)
        {
            if (arena.transform.GetChild(i).name.Equals("Center"))
            {
                arenaCenter = arena.transform.GetChild(i).gameObject;
            }
        }

        //preparation of the moves
        foreach(Move m in bossMoves)
        {
            SkillExecutor executor = (SkillExecutor)gameObject.AddComponent(Type.GetType(m.executorName));
            executor.Initialize(m);
            executors.Add(executor);
        }
    }

    private void Heuristic()
    {

    }

    public override void AgentReset()
    {
        //first reset is called before the first fight so no need to add the info to the log file
        if (notFirstReset)
        {
            //create log file
            CreateLogOfFight();
        }

        //reset log
        if(GetComponent<PersonaController>().logWindow != null)
        {
            GetComponent<PersonaController>().logWindow.GetComponent<GUILog>().ResetLog();
        }
        

        //arena reset (pillars and projectiles)
        if (arenaCenter.transform.childCount != 0)
        {
            for(int i = 0; i < arenaCenter.transform.childCount; i++)
            {
                Destroy(arenaCenter.transform.GetChild(i).gameObject);
            }
        }
        //foreach(GameObject player in players)
        //{
        //    for(int i = 0; i < player.transform.childCount; i++)
        //    {
        //        if (player.transform.GetChild(i).tag.Equals("proj"))
        //        {
        //            Destroy(player.transform.GetChild(i).gameObject);
        //        }
        //    }
        //}
        //for (int i = 0; i < gameObject.transform.childCount; i++)
        //{
        //    if (gameObject.transform.GetChild(i).tag.Equals("proj"))
        //    {
        //        Destroy(gameObject.transform.GetChild(i).gameObject);
        //    }
        //}

        //reset of the skills of the agent
        foreach (SkillExecutor executor in executors)
        {
            executor.Reset();
        }

        //reset of the agent
        gameObject.GetComponent<PersonaController>().Reset();
        pillarActivated = false;
        checkCdBA = 0;

        //reset of the other characters
        foreach(GameObject player in players)
        {
            if(player != null)
            {
                player.SetActive(true);
                player.GetComponent<PersonaController>().Reset();
            }
        }
    }

    //obeservations of the agent
    public override void CollectObservations()
    {
        //boss health
        AddVectorObs(Normalize(gameObject.GetComponent<PersonaController>().health, 0, gameObject.GetComponent<PersonaController>().MaxHealth));
        AddVectorObs(IsInSecondPhase() ? 1 : 0);

        //Boss position and facing direction
        AddVectorObs(this.transform.localPosition);
        AddVectorObs(this.transform.forward);

        //Boss velocity
        AddVectorObs(this.transform.InverseTransformDirection(rBody.velocity));

        //check if is casting
        AddVectorObs(gameObject.GetComponent<PersonaController>().isCasting ? 1 : 0);

        //boss skills check
        AddVectorObs(executors[0].CanExecute(null).Equals(ExecutorErrorCodes.OK) ? 1 : 0);
        AddVectorObs(executors[1].CanExecute(null).Equals(ExecutorErrorCodes.OK) ? 1 : 0);
        AddVectorObs(executors[2].CanExecute(null).Equals(ExecutorErrorCodes.OK) ? 1 : 0);
        AddVectorObs(executors[3].CanExecute(null).Equals(ExecutorErrorCodes.OK) ? 1 : 0);
        AddVectorObs(executors[4].CanExecute(null).Equals(ExecutorErrorCodes.OK) && IsInSecondPhase() ? 1 : 0);
        AddVectorObs(executors[5].CanExecute(null).Equals(ExecutorErrorCodes.OK) && IsInSecondPhase() ? 1 : 0);
        AddVectorObs(executors[6].CanExecute(null).Equals(ExecutorErrorCodes.OK) && IsInSecondPhase() ? 1 : 0);

        //boss sight
        float rayDistance = maxDistance;
        float[] rayAngles = { 30, 60f, 90f, 120f, 150f }; //0 is right, 90 is forward, 180 is left
        string[] detectableObjects = { "AIPlayer", "Arena" }; //array of tags to detect
        //gli ultimi 2 parametri sono gli offset che indicano altezza di partenza e di arrivo rispetto 
        //al centro dell'oggetto che fa il cast, in questo caso l'agente
        AddVectorObs(rayPerception.Perceive(rayDistance, rayAngles, detectableObjects, 0f, 0f));

        //Players positions and distances from boss
        foreach(GameObject player in players)
        {
            if(player != null)
            {
                AddVectorObs(this.transform.InverseTransformPoint(player.transform.position).normalized);
                AddVectorObs(Normalize(Vector3.Distance(this.transform.position, player.transform.position), 0, maxDistance));
            }
            else
            {
                AddVectorObs(Vector3.zero);
                AddVectorObs(0);
            }
        }

        //players health
        foreach(GameObject player in players)
        {
            if(player != null)
            {
                AddVectorObs(Normalize(player.GetComponent<PersonaController>().health, 0, player.GetComponent<PersonaController>().MaxHealth));
            }
            else
            {
                AddVectorObs(0);
            }
        }
    }

    //agents actions
    public override void AgentAction(float[] vectorAction, string textAction)
    {
        //movement
        Vector3 direction = Vector3.zero;
        Vector3 rotation = Vector3.zero;

        direction = gameObject.transform.forward * Mathf.Clamp(vectorAction[0], -1, 1);

        if(direction.magnitude >= 1) //incitamento a muoversi
        {
            AddReward(0.001f);
        }

        rotation = gameObject.transform.up * Mathf.Clamp(vectorAction[1], -1, 1);

        gameObject.GetComponent<PersonaController>().MoveBoss(direction, rotation);

        //attacks
        bool attack1 = Mathf.Clamp(vectorAction[2], -1, 1) > 0;
        bool attack2 = Mathf.Clamp(vectorAction[3], -1, 1) > 0;
        bool attack3 = Mathf.Clamp(vectorAction[4], -1, 1) > 0;
        bool attack4 = Mathf.Clamp(vectorAction[5], -1, 1) > 0;
        bool attack5 = Mathf.Clamp(vectorAction[6], -1, 1) > 0;
        bool attack6 = Mathf.Clamp(vectorAction[7], -1, 1) > 0;

        if (Time.time > checkCdBA)
        {
            if (attack1) //basic melee slash
            {
                //GetComponent<PersonaController>().animator.SetBool("slash", true); //prova
                if (GetComponent<PersonaController>().logWindow != null && executors[0].CanExecute(null) == ExecutorErrorCodes.OK)
                {
                    GetComponent<PersonaController>().logWindow.GetComponent<GUILog>().AddLog("Boss uses " + bossMoves[0].moveName);
                }
                executors[0].ExecuteSkill(null);
            }

            if (attack2) //basic ranged attack
            {
                if (GetComponent<PersonaController>().logWindow != null && executors[1].CanExecute(null) == ExecutorErrorCodes.OK)
                {
                    GetComponent<PersonaController>().logWindow.GetComponent<GUILog>().AddLog("Boss uses " + bossMoves[1].moveName);
                }
                executors[1].ExecuteSkill(null);
            }

            if (attack3) //rotSlash
            {
                if (GetComponent<PersonaController>().logWindow != null && executors[2].CanExecute(null) == ExecutorErrorCodes.OK)
                {
                    GetComponent<PersonaController>().logWindow.GetComponent<GUILog>().AddLog("Boss uses " + bossMoves[2].moveName);
                }
                executors[2].ExecuteSkill(null);
            }

            if (attack4) //basic AOE
            {
                if (GetComponent<PersonaController>().logWindow != null && executors[3].CanExecute(null) == ExecutorErrorCodes.OK)
                {
                    GetComponent<PersonaController>().logWindow.GetComponent<GUILog>().AddLog("Boss uses " + bossMoves[3].moveName);
                }
                executors[3].ExecuteSkill(null);
            }

            if (attack5 && pillarActivated) //AOE force
            {
                if (GetComponent<PersonaController>().logWindow != null && executors[4].CanExecute(null) == ExecutorErrorCodes.OK)
                {
                    GetComponent<PersonaController>().logWindow.GetComponent<GUILog>().AddLog("Boss uses " + bossMoves[4].moveName);
                }
                executors[4].ExecuteSkill(null);
            }

            if (attack6 && pillarActivated) //AOE pattern
            {
                if (GetComponent<PersonaController>().logWindow != null && executors[5].CanExecute(null) == ExecutorErrorCodes.OK)
                {
                    GetComponent<PersonaController>().logWindow.GetComponent<GUILog>().AddLog("Boss uses " + bossMoves[5].moveName);
                }
                executors[5].ExecuteSkill(null);
            }

            checkCdBA = Time.time + cdBetweenAttacks;
        }



        //agent done is every player is defeated
        bool allDefeated = true;
        foreach(GameObject player in players)
        {
            if (player != null && player.activeSelf)
            {
                allDefeated = false;
            }
        }
        if (allDefeated)
        {
            AddReward(1f);
            notFirstReset = true;
            Done();
        }

        //agent done if he is defeated
        if (gameObject.GetComponent<PersonaController>().health <= 0)
        {
            AddReward(-1f);
            notFirstReset = true;
            Done();
        }

        if(gameObject.transform.position.y < 0)
        {
            Done();
        }
    }

    //method to normalize values
    private float Normalize(float value, float min, float max)
    {
        float result = 0f;

        result = (value - min) / (max - min);

        return result;
    }

    //method to check if boss has 50% or less health to let him use the second phase moves
    private bool IsInSecondPhase()
    {
        float maxHealth = gameObject.GetComponent<PersonaController>().MaxHealth;
        float currentHealth = gameObject.GetComponent<PersonaController>().health;

        if(currentHealth <= (maxHealth * 50) / 100)
        {
            return true;
        }

        return false;
    }

    public void SecondPhase()
    {
        if(GetComponent<PersonaController>().logWindow != null)
        {
            GetComponent<PersonaController>().logWindow.GetComponent<GUILog>().AddLog("Boss uses " + bossMoves[6].moveName);
        }
        if (executors[6].ExecuteSkill(null)) //pillars
        {
            pillarActivated = true;
        }
    }

    private void CreateLogOfFight()
    {
        GameObject academy = GameObject.FindGameObjectWithTag("academy");

        //duration of the fight
        fightDuration = Time.time - tmpDuration;
        tmpDuration = Time.time;

        string info = "";

        Dictionary<int, float> lifes = new Dictionary<int, float>();
        int index = 0;
        if(gameObject.GetComponent<PersonaController>().health <= 0)
        {
            info = info + "\nDEFEAT: Boss Life: 0";

            lifes.Add(index, 0);
            index++;

            foreach (GameObject player in players)
            {
                if(player != null)
                {
                    info = info + "\n\t " + player.GetComponent<PersonaController>().characterCategory.ToString() + " Life: " + player.GetComponent<PersonaController>().health;
                    lifes.Add(index, player.GetComponent<PersonaController>().health);
                    index++;
                }
                
            }

            academy.GetComponent<BossFightAcademy>().AddLogInfo(info, fightDuration, false, lifes);
        }
        else
        {
            index = 0;
            info = info + "\nWIN: Boss Life: " + gameObject.GetComponent<PersonaController>().health;

            lifes.Add(index, gameObject.GetComponent<PersonaController>().health);
            index++;

            foreach (GameObject player in players)
            {
                if(player != null)
                {
                    info = info + "\n\t " + player.GetComponent<PersonaController>().characterCategory.ToString() + " Life: " + player.GetComponent<PersonaController>().health;
                    lifes.Add(index, 0);
                    index++;
                }
                
            }

            academy.GetComponent<BossFightAcademy>().AddLogInfo(info, fightDuration, true, lifes);
        }
    }
}
