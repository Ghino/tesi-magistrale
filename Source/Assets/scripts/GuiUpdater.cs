﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class GuiUpdater : MonoBehaviour
{
    public GameObject character;
    private string characterName;
    private float maxHealth;

    // Start is called before the first frame update
    void Start()
    {
        CharacterCategory category = character.GetComponent<PersonaController>().characterCategory;
        characterName = category.ToString();
        maxHealth = character.GetComponent<PersonaController>().health;
        gameObject.GetComponent<Text>().text = characterName;
        GameObject charIcon = gameObject.transform.GetChild(1).gameObject;

        #if UNITY_EDITOR

        switch (category)
        {
            case CharacterCategory.Boss:
                charIcon.GetComponent<Image>().sprite = (Sprite)AssetDatabase.LoadAssetAtPath("Assets/images/bossIcon.png", typeof(Sprite));
                break;
            case CharacterCategory.Tank:
                charIcon.GetComponent<Image>().sprite = (Sprite)AssetDatabase.LoadAssetAtPath("Assets/images/tankIcon.png", typeof(Sprite));
                break;
            case CharacterCategory.Healer:
                charIcon.GetComponent<Image>().sprite = (Sprite)AssetDatabase.LoadAssetAtPath("Assets/images/healerIcon.png", typeof(Sprite));
                break;
            case CharacterCategory.MeleeDPS:
                charIcon.GetComponent<Image>().sprite = (Sprite)AssetDatabase.LoadAssetAtPath("Assets/images/meleeDPSIcon.png", typeof(Sprite));
                break;
            case CharacterCategory.RangedDPS:
                charIcon.GetComponent<Image>().sprite = (Sprite)AssetDatabase.LoadAssetAtPath("Assets/images/rangedDPSIcon.png", typeof(Sprite));
                break;
        }

#else

        switch (category)
        {
            case CharacterCategory.Boss:
                charIcon.GetComponent<Image>().sprite = Resources.Load<Sprite>(Application.dataPath + "bossIcon.png");
                break;
            case CharacterCategory.Tank:
                charIcon.GetComponent<Image>().sprite = Resources.Load<Sprite>(Application.dataPath + "tankIcon.png");
                break;
            case CharacterCategory.Healer:
                charIcon.GetComponent<Image>().sprite = Resources.Load<Sprite>(Application.dataPath + "healerIcon.png");
                break;
            case CharacterCategory.MeleeDPS:
                charIcon.GetComponent<Image>().sprite = Resources.Load<Sprite>(Application.dataPath + "meleeDPSIcon.png");
                break;
            case CharacterCategory.RangedDPS:
                charIcon.GetComponent<Image>().sprite = (Sprite)Resources.Load(Application.dataPath + "rangedDPSIcon.png", typeof(Sprite));
                break;
        }

#endif
    }

    // Update the life bars of the characters
    void FixedUpdate()
    {
        float health = 0;
        health = character.GetComponent<PersonaController>().health;
        //gameObject.GetComponent<Text>().text = characterName + " " + Mathf.RoundToInt(health);

        GameObject healthBar = gameObject.transform.GetChild(0).GetChild(0).gameObject;
        healthBar.GetComponent<Image>().fillAmount = (health - 0) / (maxHealth - 0);
    }
}
