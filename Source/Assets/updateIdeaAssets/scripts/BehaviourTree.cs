﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//delegate for actions and condition
public delegate bool BTCall();

//uniform abstract class for all the different tasks
public abstract class IBTTask
{
    //0 fail
    //1 success
    //-1 try again
    public abstract int Run();
}

//a task which check a specific condition
public class BTCondition : IBTTask
{
    public BTCall Condition;

    public BTCondition(BTCall call)
    {
        Condition = call;
    }

    public override int Run()
    {
        return Condition() ? 1 : 0;
    }
}

//a task which executes an action
public class BTAction : IBTTask
{
    public BTCall Action;

    public BTAction(BTCall call)
    {
        Action = call;
    }

    public override int Run()
    {
        return Action() ? 1 : 0;
    }
}

//hold and compose subtasks
public abstract class BTComposite : IBTTask
{
    protected IBTTask[] children;

    public BTComposite(IBTTask[] tasks)
    {
        children = tasks;
    }

    public abstract override int Run();
}

//task implementing a selector
public class BTSelector : BTComposite
{
    private int index = 0;

    public BTSelector(IBTTask[] tasks) : base(tasks)
    {

    }

    //all children run until one succeeds
    public override int Run()
    {
        while (index < children.Length)
        {
            int v = children[index].Run();
            if (v == -1)
            {
                return -1;
            }
            if (v == 0)
            {
                index += 1;
                return -1;
            }
            if (v == 1)
            {
                index = 0;
                return 1;
            }
        }

        //selector fails
        index = 0;
        return 0;
    }
}

//task implementing a sequence
public class BTSequence : BTComposite
{
    private int index = 0;

    public BTSequence(IBTTask[] tasks) : base(tasks)
    {

    }

    //all children run until one fails
    public override int Run()
    {
        while (index < children.Length)
        {
            int v = children[index].Run();
            if (v == -1)
            {
                return -1;
            }
            if (v == 0)
            {
                index = 0;
                return 0;
            }
            if (v == 1)
            {
                index += 1;
                return -1;
            }
        }

        //sequence succeeds
        index = 0;
        return 1;
    }
}

//abstract class holding a decorator
public abstract class BTDecorator : IBTTask
{
    protected IBTTask Child;

    public BTDecorator(IBTTask task)
    {
        Child = task;
    }

    public abstract override int Run();
}

//executes child task if condition is verified
public class BTDecoratorFilter : BTDecorator
{
    private BTCall Condition;

    public BTDecoratorFilter(BTCall condition, IBTTask task) : base(task)
    {
        Condition = condition;
    }

    public override int Run()
    {
        return Condition() ? Child.Run() : 0;
    }
}

//repeat task until x times
public class BTDecoratorLimit : BTDecorator
{
    public int maxRepetitions;
    public int count;

    public BTDecoratorLimit(int max, IBTTask task) : base(task)
    {
        maxRepetitions = max;
        count = 0;
    }

    public override int Run()
    {
        if (count >= maxRepetitions)
        {
            return 0;
        }

        int v = Child.Run();
        if (v != -1)
        {
            count += 1;
        }
        return v;
    }
}

//repeat until fail
public class BTDecoratorUntilFail : BTDecorator
{
    public BTDecoratorUntilFail(IBTTask task) : base(task)
    {

    }

    public override int Run()
    {
        if (Child.Run() != 0)
        {
            return -1;
        }
        return 1;
    }
}

//inverts the result of a task
public class BTDecoratorInverter : BTDecorator
{
    public BTDecoratorInverter(IBTTask task) : base(task)
    {

    }

    public override int Run()
    {
        int v = Child.Run();
        if (v == 1)
        {
            return 0;
        }
        if (v == 0)
        {
            return 1;
        }

        //-1
        return v;
    }
}

//implementation of the BT
public class BehaviorTree
{
    public IBTTask root;

    public BehaviorTree(IBTTask task)
    {
        root = task;
    }

    public bool Step()
    {
        return root.Run() < 0 ? true : false;
    }
}