﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testExecutor : MonoBehaviour
{
    public Move m;
    public GameObject healed;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.transform.LookAt(healed.transform.position);
        SkillExecutor executor = (SkillExecutor)gameObject.AddComponent(Type.GetType(m.executorName));
        executor.Initialize(m);
        List<GameObject> targets = new List<GameObject>();
        targets.Add(healed);
        //Debug.Log(Time.time);
        //Debug.Log(executor.CanExecute(targets));
        executor.ExecuteSkill(targets);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
