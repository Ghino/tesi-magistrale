﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testVFXProj : MonoBehaviour
{
    public Move move;
    private float cd = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0) && Time.time > cd)
        {
            GameObject projectile = Instantiate(move.projectile, gameObject.transform.position, gameObject.transform.rotation);
            projectile.GetComponent<ProjectileController>().Initialize(move, gameObject);

            cd = Time.time + move.cooldown;
        }
    }
}
