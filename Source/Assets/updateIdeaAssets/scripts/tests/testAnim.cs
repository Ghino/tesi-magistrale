﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testAnim : MonoBehaviour
{
    private Animator animator;
    private Rigidbody rBody;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        rBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            rBody.velocity = transform.forward * 10;
            animator.SetFloat("Speed", 0.5f);
        }
    }
}
