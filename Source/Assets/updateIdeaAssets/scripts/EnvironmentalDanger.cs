﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentalDanger : MonoBehaviour
{
    public float triggerTime = 1f;
    private bool isTriggered = false;
    private float timer = 0;
    private Color baseColor;
    private List<GameObject> charactersInside = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        baseColor = gameObject.GetComponent<MeshRenderer>().material.color;
    }

    void FixedUpdate()
    {
        if (isTriggered)
        {
            //placeholder to visually check the passing of time between the trigger and the actual activation of the effect
            Color matColor = gameObject.GetComponent<MeshRenderer>().material.color;

            matColor.a += Time.deltaTime * triggerTime;

            gameObject.GetComponent<MeshRenderer>().material.color = baseColor;

            //after triggerTime the effect of the danger is activated
            if (Time.time > timer)
            {
                gameObject.GetComponent<MeshRenderer>().material.color = baseColor;

                for (int i = charactersInside.Count - 1; i >= 0; i--)
                {
                    charactersInside[i].GetComponent<PersonaController>().AddHit(30, "area hits");
                }

                isTriggered = false;
            }
            
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //counts every character who enters the danger zone
        Debug.Log(other.tag);
        if(other.gameObject.tag.Equals("AIPlayer") || other.gameObject.tag.Equals("Boss"))
        {
            Debug.Log("qui");
            charactersInside.Add(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //remove every character who leaves the danger zone
        if (other.gameObject.tag.Equals("AIPlayer") || other.gameObject.tag.Equals("Boss"))
        {
            for(int i = charactersInside.Count - 1; i >= 0; i--)
            {
                if (charactersInside[i].name.Equals(other.gameObject.name))
                {
                    Debug.Log("qui!");
                    charactersInside.RemoveAt(i);
                }
            }
        }
    }

    //activate the effect
    public void Trigger()
    {
        isTriggered = true;
        timer = Time.time + triggerTime;
    }
}
