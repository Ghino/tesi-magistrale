﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolController : MonoBehaviour
{
    public float duration = 10;
    public int dps = 10;

    private string parentTag;
    private int counter = 0;
    private float timer = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Initialize(GameObject parent)
    {
        this.parentTag = parent.tag;
    }

    void FixedUpdate()
    {
        duration -= Time.deltaTime;

        if(duration <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.tag.Equals(parentTag))
        {
            if (other.gameObject.tag.Equals("Boss") || other.gameObject.tag.Equals("AIPlayer"))
            {
                if (counter == 0)
                {
                    timer = Time.time;
                }
                counter++;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (!other.gameObject.tag.Equals(parentTag))
        {
            if (Time.time > timer && timer != 0)
            {
                timer = Time.time + 1;

                if (other.gameObject.tag.Equals("Boss"))
                {
                    other.gameObject.GetComponent<PersonaController>().AddHit(dps, null);
                }
                if (other.gameObject.tag.Equals("AIPlayer"))
                {
                    other.gameObject.GetComponent<PersonaController>().AddHit(dps, null);
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (!other.gameObject.tag.Equals(parentTag))
        {
            if (other.gameObject.tag.Equals("Boss") || other.gameObject.tag.Equals("AIPlayer"))
            {
                counter--;
                if (counter <= 0)
                {
                    timer = 0;
                }
            }
        }
    }


}
