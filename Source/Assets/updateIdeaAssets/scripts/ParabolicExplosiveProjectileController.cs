﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParabolicExplosiveProjectileController : MonoBehaviour
{
    public float shootAngle = 30;
    public GameObject explosive;


    private GameObject parent;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Initialize(GameObject parent, GameObject target, Move move)
    {
        this.parent = parent;
        gameObject.GetComponent<Rigidbody>().velocity = BallisticVel(target.transform, shootAngle);
    }

    void FixedUpdate()
    {
        if(gameObject.transform.position.y < 0)
        {
            Destroy(gameObject);
        }
    }

    private Vector3 BallisticVel(Transform target, float angle){
        // get target direction
        Vector3 dir = target.position - transform.position;

        // get height difference
        float h = dir.y;

        // retain only the horizontal direction
        dir.y = 0;

        // get horizontal distance
        float dist = dir.magnitude;

        // convert angle to radians
        float a = angle * Mathf.Deg2Rad;

        // set dir to the elevation angle
        dir.y = dist * Mathf.Tan(a);

        // correct for small height differences
        dist += h / Mathf.Tan(a);

        // calculate the velocity magnitude
        float vel = Mathf.Sqrt(dist * Physics.gravity.magnitude / Mathf.Sin(2 * a));

        return vel * dir.normalized;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.tag.Equals(parent.tag))
        {
            GameObject explosion = Instantiate(explosive, new Vector3(gameObject.transform.position.x, 0.1f, gameObject.transform.position.z), Quaternion.identity);
            explosion.GetComponent<PoolController>().Initialize(parent);
            Destroy(gameObject);
        }
    }
}
