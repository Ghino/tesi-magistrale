﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableObj : MonoBehaviour
{
    public float interactionCooldown = 10f;
    public List<GameObject> areasToActivate;
    private GameObject joint;
    private float timer = 0;
    // Start is called before the first frame update
    void Start()
    {
        //knowing the prefab's children order i can get the joint
        joint = gameObject.transform.GetChild(1).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        //if one of the characters in the arena (the boss or the players) goes close to the object, activate it
        if(other.gameObject.tag.Equals("AIPlayer") || other.gameObject.tag.Equals("Boss"))
        {
            //timer to avoid multiple input at the same time
            if(Time.time >= timer)
            {
                timer = Time.time + interactionCooldown;
                joint.GetComponent<Animator>().SetTrigger("used");

                //the effect
                foreach (GameObject area in areasToActivate)
                {
                    area.GetComponent<EnvironmentalDanger>().Trigger();
                }
            }
        }
    }
}
